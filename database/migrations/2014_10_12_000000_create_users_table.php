<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Usuarios
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de usuarios
|
*/
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc En esta clase es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar métodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateUsersTable extends Migration
{
    /** 
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla con los campos a utilizar
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */  
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('apellidos',50);
            $table->string('domicilio',100);
            $table->string('curp',30);
            $table->string('rfcCliente',20);
            $table->string('telefono',15);
            $table->string('email',100)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('users');
    }//.down

}//.CreateUsersTable
