<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Mascotas
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de Mascotas
|
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateMascotasTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('mascotas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',30);
            $table->float('peso');//precision y decimales.
            $table->integer('edad');
            $table->boolean('estatus')->default(1);
            $table->unsignedBigInteger('id_users');
            $table->unsignedBigInteger('id_raza');
            $table->timestamps();
            $table->foreign('id_users')->references('id')->on('users');
            $table->foreign('id_raza')->references('id')->on('cat_razas');
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('mascotas');
    }//.down
}//.CreateMascotasTable
