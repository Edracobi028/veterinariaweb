<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Pedidos
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de Pedidos
|
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreatePedidosTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('pedidos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipoPago',40);
            $table->string('estado',30);
            $table->string('idPago');
            $table->float('subtotal');
            $table->float('total');
            $table->float('descuento');
            $table->string('estadoPago');
            $table->unsignedBigInteger('id_users');
            $table->timestamps();
            $table->foreign('id_users')->references('id')->on('users');
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('pedidos');
    }//.down
}//.CreatePedidosTable
