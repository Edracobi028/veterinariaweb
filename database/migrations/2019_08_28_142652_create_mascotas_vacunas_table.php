<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Mascotas_Vacunas
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de 
| muchos a muchos Mascotas_Vacunas.
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateMascotasVacunasTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('mascotas_vacunas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->float('costo');
            $table->string('observaciones')->nullable();
            $table->boolean('estatus')->default(1);
            $table->unsignedBigInteger('id_mascota');
            $table->unsignedBigInteger('id_vacuna');
            $table->timestamps();
            $table->foreign('id_mascota')->references('id')->on('mascotas');
            $table->foreign('id_vacuna')->references('id')->on('cat_vacunas');
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('mascotas_vacunas');
    }//.down

}//.CreateMascotasVacunasTable
