<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Proveedores
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de Proveedores
|
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateProveedoresTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('proveedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',30);
            $table->string('domicilio',100);
            $table->string('rfcProveedor',30);
            $table->string('telefono',15);
            $table->string('correo',50);
            $table->boolean('estatus')->default(1);
            $table->timestamps();
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('proveedores');
    }//.down
}//.CreateProveedoresTable
