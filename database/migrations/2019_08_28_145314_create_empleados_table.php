<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Empleados
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de Empleados
|
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateEmpleadosTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('empleados', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',30);
            $table->string('apellidos',50);
            $table->string('domicilio',100);
            $table->float('sueldo');
            $table->string('nss',10);
            $table->string('curp',30);
            $table->string('correo',50);
            $table->string('password',255);
            $table->boolean('estatus')->default(1);
            $table->unsignedBigInteger('id_puesto');
            $table->timestamps();
            $table->foreign('id_puesto')->references('id')->on('cat_puestos');
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('empleados');
    }//.down
}//.CreateEmpleadosTable
