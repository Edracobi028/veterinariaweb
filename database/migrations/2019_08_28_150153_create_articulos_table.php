<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla Artículos
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de Artículos
|
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateArticulosTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',50); //aumentar la longitud de nombre articulo
            $table->string('descripcion');
            $table->string('sku',30);
            $table->float('precio_venta');
            $table->float('impuestos');
            $table->string('ubicacion',10);
            $table->string('imagen');
            $table->string('tipoUnidad',30);
            $table->Integer('unidadMax');
            $table->Integer('existencias');
            $table->boolean('estatus')->default(1);
            $table->unsignedBigInteger('id_empleado');
            $table->unsignedBigInteger('id_marca');
            $table->unsignedBigInteger('id_presentacion');
            $table->unsignedBigInteger('id_departamento');                             
            $table->timestamps(); //metodo para crear campo created_at updated_at
            $table->foreign('id_empleado')->references('id')->on('empleados');
            $table->foreign('id_marca')->references('id')->on('cat_marcas');
            $table->foreign('id_presentacion')->references('id')->on('cat_presentaciones');
            $table->foreign('id_departamento')->references('id')->on('cat_departamentos');
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('articulos');
    }//.down

}//.CreateArticulosTable
