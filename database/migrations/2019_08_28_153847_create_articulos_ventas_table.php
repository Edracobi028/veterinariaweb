<?php
/*
|--------------------------------------------------------------------------
| Migración Tabla  Artículos_Ventas
|--------------------------------------------------------------------------
|
| Este archivo es para crear y administrar los esquemas para la tabla de 
| muchos a muchos Artículos_Ventas
*/

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * @desc Aqui es donde vamos a definir los esquemas para nuestra base de datos,
 * puedes encontrar metodos como up(), down().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CreateArticulosVentasTable extends Migration
{
    /**
     * Corre las migraciones
     * @return void
     * @desc Método para crear la estructura de una tabla
     */ 
    public function up()
    {
        /* Crear la tabla definiendo su nombre, campos y su tipo de dato */
        Schema::create('articulos_ventas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->Integer('cantidad');
            $table->float('subtotal');
            $table->unsignedBigInteger('id_venta');
            $table->unsignedBigInteger('id_articulo');
            $table->timestamps();
            $table->foreign('id_venta')->references('id')->on('ventas');
            $table->foreign('id_articulo')->references('id')->on('articulos');
        });
    }//.up

    /**
     * Revertir las migraciones.
     * @return void
     * @description Método para  eliminar la tabla
     */
    public function down()
    {
        /* Eliminar la tabla pasando el titulo de la misma*/
        Schema::dropIfExists('articulos_ventas');
    }//.down
}//.CreateArticulosVentasTable
