<?php
/*
|--------------------------------------------------------------------------
| Seeder Empleados
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

use Illuminate\Database\Seeder;

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class empleados_Seeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        DB::table('empleados')->truncate();           //utilizar el método truncate, que se encarga de vaciar la table
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

        /* crear un array para insertar informacion a una tabla */
        $data = array(
        
            [
                'nombre'     => 'Eduardo Razo',
                'apellidos'  => 'Cobian',
                'domicilio'  => 'Loma linda 88',
                'sueldo'     =>  1200,
                'nss'        => 'TOHANSS123',
                'curp'       => 'TOHACURP1234',
                'correo'     => 'razo@gmail.com',
                'password'   => '12345678',
                'estatus'    => 1,
                'id_puesto'  => 1,
                
            ],
        );

        /*Insertar el array a travez del modelo*/  
        DB::table('empleados')->insert($data);

    }//.run
}//.empleados_Seeder
