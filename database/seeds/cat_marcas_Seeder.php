<?php
/*
|--------------------------------------------------------------------------
| Seeder Marcas
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class cat_marcas_Seeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     *
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        DB::table('cat_marcas')->truncate();          // utilizar el método truncate, que se encarga de vaciar la table
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

        /* crear un array para insertar informacion a una tabla */
        $data = array(
        
            [
                'nombre'     => 'Dogline',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nombre'     => 'Nerf Dog ',
                'estatus'    => 1,    
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'nombre'     => 'Diamond',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),    
            ],
            [
                'nombre'     => 'PetStar',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),    
            ],
            [
                'nombre'     => 'Pet Safe',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),    
            ],
            [
                'nombre'     => 'Minino',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),    
            ],
            [
                'nombre'     => 'Cat´s Best',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),    
            ],
            [
                'nombre'     => 'Un Dos Treat',
                'estatus'    => 1,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),    
            ],
    
        );

        /*Insertar el array a travez del modelo*/  
        DB::table('cat_marcas')->insert($data);

    }//.run
}//.cat_marcas_Seeder
