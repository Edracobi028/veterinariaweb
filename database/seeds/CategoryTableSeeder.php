<?php
/*
|--------------------------------------------------------------------------
| Seeder Categorias
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Category;

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CategoryTableSeeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     *
     * @return void
     */
    public function run()
    {      
        /* crear un array para insertar informacion a una tabla */
        $data = array(
            [
                'name' => 'Super heroes',
                'slug' => 'super heroes',
                'description' => 'Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, Tempore, perferendis!',
                'color' => '#440022'
            ],
            [
                'name' => 'Geek',
                'slug' => 'geek',
                'description' => 'Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit, Tempore, perferendis!',
                'color' => '#445500'
            ]
        );
        
        /*Insertar el array a travez del modelo*/ 
        Category::insert($data);

    }//.run
}//.CategoryTableSeeder