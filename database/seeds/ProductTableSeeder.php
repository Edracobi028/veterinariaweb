<?php
/*
|--------------------------------------------------------------------------
| Seeder Artículos
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Product;

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class ProductTableSeeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     *
     * @return void
     */
    public function run()
    {   
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');  // Desactivamos la revisión de claves foráneas
        DB::table('articulos')->truncate();            // Método truncate, que se encarga de vaciar la tabla
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');  // Reactivamos la revisión de claves foráneas
        
        /* crear un array para insertar informacion a una tabla */
        $data = array(
            [
                'nombre'          =>'Tapete Entrenador Green Carpet',
                'descripcion'     =>'El tapete entrenador para que los perros orinen, es un baño portátil para tu mascota.',
                'sku'             =>'GRETAPMED',
                'precio_venta'    =>629.00,
                'impuestos'       =>18.87,
                'ubicacion'       =>'A1',
                'imagen'          =>'/veterinaria/images/articulos/tapedeMediano.jpg',
                'tipoUnidad'      =>'PZA',
                'unidadMax'       =>10,
                'existencias'     =>9,
                'estatus'         =>1, 
                'id_empleado'     =>1,
                'id_marca'        =>1,
                'id_presentacion' =>1,
                'id_departamento' =>1,
            ],
        
            [
                'nombre'          =>'Collar de Microfibra - Chico',
                'descripcion'     =>'El Collar de Microfibra de Dogline ideal para tu perro por su resistencia y comodidad. La microfibra es un nuevo material en la industria de las mascotas y en estos collares se siente como gamuza ultra suave además de su resistencia para lavar a máquina.',
                'sku'             =>'COLLDOGLCHIC',
                'precio_venta'    =>135.00,
                'impuestos'       =>20.25,
                'ubicacion'       =>'A1',
                'imagen'          =>'/veterinaria/images/articulos/collarChico2.jpg',
                'tipoUnidad'      =>'PZA',
                'unidadMax'       =>10,
                'existencias'     =>9,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>1, //1=DOGLINE 2=NERFDOG 3= DIAMOND
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
          
            [
                'nombre'          =>'Disco Nylon Flying Disc',
                'descripcion'     =>'Nylon Flying Disc es un frisbee interactivo para el desempeño de perros muy activos.',
                'sku'             =>'DISCNERFAZUL',
                'precio_venta'    =>145.00,
                'impuestos'       =>21.75,
                'ubicacion'       =>'A1',
                'imagen'          =>'/veterinaria/images/articulos/discoNerfAzul.jpg',
                'tipoUnidad'      =>'PZA',
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>2, //1=DOGLINE 2=NERFDOG 3= DIAMOND
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [
                'nombre'          =>'Arnés Unimax - Mediano', //30
                'descripcion'     =>'El Arnés para Perros Unimax  de Dogline es una pechera multiusos: es perfecto para perros de servicio o simplemente para caminar y hacer ejercicio.',
                'sku'             =>'ARNSUNMXMDNG',
                'precio_venta'    =>666.00,
                'impuestos'       =>99.9,
                'ubicacion'       =>'A1',
                'imagen'          =>'/veterinaria/images/articulos/arnesMediana.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>1, //1=DOGLINE 2=NERFDOG 3= DIAMOND
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //---------------------------- 
                'nombre'          =>'Correa de Microfibra - Chico', //30
                'descripcion'     =>'La correa Dogline ideal para tu perro por su resistencia y comodidad. La microfibra es un nuevo material en la industria de las mascotas y en estas correas se siente como gamuza ultra suave además de que es resistente para lavar a máquina.',
                'sku'             =>'CORRDGLNCHNO',
                'precio_venta'    =>175.00,
                'impuestos'       =>26.25,
                'ubicacion'       =>'A2',
                'imagen'          =>'/veterinaria/images/articulos/correachica2.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>10,
                'existencias'     =>9,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>1, //1=DOGLINE 2=NERFDOG 3= DIAMOND
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //---------------------------- 
                'nombre'          =>'Mini Bones de Cerdo para Perro', //30
                'descripcion'     =>'Los MINI BONES son una excelente fuente natural de calcio que proporciona una correcta higiene dental, no solo le regalas a tu mascota un entretenido juguete sino que, además, disfrutará durante horas del sabor de un producto saludable y nutritivo.',
                'sku'             =>'MNBNPTSRMIN3',
                'precio_venta'    =>90.00,
                'impuestos'       =>13.5,
                'ubicacion'       =>'A2',
                'imagen'          =>'/veterinaria/images/articulos/miniBone.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>10,
                'existencias'     =>9,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>4, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR
                'id_presentacion' =>3, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //---------------------------- 
                'nombre'          =>'Porky Pop de Cerdo para Perros', //30
                'descripcion'     =>'El Porky Pop de cerdo ahumado son una excelente fuente natural de calcio que proporciona una correcta higiene dental. No solo le regalas a tu mascota un entretenido juguete tambien, disfrutará del sabor de un producto saludable y nutritivo.',
                'sku'             =>'PRKPPTSRGRDE',
                'precio_venta'    =>75.00,
                'impuestos'       =>11.25,
                'ubicacion'       =>'A2',
                'imagen'          =>'/veterinaria/images/articulos/porkyPop.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>10,
                'existencias'     =>9,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>4, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //---------------------------- 
                'nombre'          =>'Puppy', //30
                'descripcion'     =>'Diamond Puppy contiene el equilibrio adecuado de grasa, proteína y otros nutrientes esenciales para que tu cachorro reciba los nutrientes apropiados para crecer fuerte y sano. ',
                'sku'             =>'MNBNPTSRMIN3',
                'precio_venta'    =>115.00,
                'impuestos'       =>17.25,
                'ubicacion'       =>'A2',
                'imagen'          =>'/veterinaria/images/articulos/DIAMOND_PUPPY.jpg',
                'tipoUnidad'      =>'KG', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>3, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR
                'id_presentacion' =>3, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //---------------------------- 
                'nombre'          =>'Arena Scoop Free', //30
                'descripcion'     =>'La Arena Scoop Free es una bandeja desechable, hecha con materiales reciclados. Cada bandeja ha sido cargada con Cristales Blancos libres de perfumes y colorantes e incluyen una bolsa para desecharlas fácilmente.',
                'sku'             =>'ARENPTSFCAFE',
                'precio_venta'    =>262.00,
                'impuestos'       =>39.3,
                'ubicacion'       =>'A3',
                'imagen'          =>'/veterinaria/images/articulos/arenero.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>5, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>'Plato Anti-Hormiga Cats in Love 210 ml', //30
                'descripcion'     =>'Plato con excelente estabilidad debido a que cuenta con una goma en la parte inferior para evitar el deslizamiento. Cuenta con un riel donde al colocar un poco de agua evita que las hormigas avancen hasta las croquetas.',
                'sku'             =>'PLATPTSFB210',
                'precio_venta'    =>154.00,
                'impuestos'       =>23.1,
                'ubicacion'       =>'A3',
                'imagen'          =>'/veterinaria/images/articulos/platoGato.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>5, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>'Minino Plus 1.3 Kg', //30
                'descripcion'     =>'Minino Plus esta elaborado con una selección de carnes (res, pollo y pavo) e ingredientes de la más alta calidad que le garantizan nutrición completa y balanceada a tu gato asegurando su sano desarrollo durante toda su vida adulta.',
                'sku'             =>'CROQMINO1_3K',
                'precio_venta'    =>65.00,
                'impuestos'       =>23.1,
                'ubicacion'       =>'A3',
                'imagen'          =>'/veterinaria/images/articulos/minino1_3kg.jpg',
                'tipoUnidad'      =>'KG', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>6, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 6=MININO 
                'id_presentacion' =>3, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>'Ratón de Fieltro con Movimiento', //30
                'descripcion'     =>'Ratón de Fieltro con Movimiento. Ya sea hecho de peluche o tejido, en una cuerda, o con muelle: los juguetes de gato Trixie ofrecen todo para todos los tipos de juego.',
                'sku'             =>'RATNPTSFROJO',
                'precio_venta'    =>82.00,
                'impuestos'       =>12.3,
                'ubicacion'       =>'A3',
                'imagen'          =>'/veterinaria/images/articulos/ratonFieltro.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>5, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 6=MININO 
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>'Set de Pelotas de Juego', //30
                'descripcion'     =>'Set de Pelotas de Juego. Ya sea hecho de peluche o tejido, en una cuerda, o con muelle: los juguetes de gato Pet Safe ofrecen todo para todos los tipos de juego.',
                'sku'             =>'BOLAPTSFAZUL',
                'precio_venta'    =>65.00,
                'impuestos'       =>9.75,
                'ubicacion'       =>'A4',
                'imagen'          =>'/veterinaria/images/articulos/bolaAzul.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>5, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 6=MININO 
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>"Arena para Gatos Cat's Best", //30
                'descripcion'     =>"La arena para Gatos Cat's Best Öko Plus está hecha de fibras orgánicas 100% naturales; debido a esto crea el efecto de 'aglutinamiento' sin ayuda de químicos o agentes artificiales. ",
                'sku'             =>'ARENCATB2_3K',
                'precio_venta'    =>139.00,
                'impuestos'       =>20.85,
                'ubicacion'       =>'A4',
                'imagen'          =>'/veterinaria/images/articulos/arenaGatos.jpg',
                'tipoUnidad'      =>'KG', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>7, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 6=MININO 7=Cat´s Best
                'id_presentacion' =>3, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>"Cascabel de metal", //30
                'descripcion'     =>"La seguridad es primero: Pet Safe te ofrece una amplia gama de productos que ayudan a proteger a tu mascota. Este cascabel se puede ajustar al collar de su gato con facilidad, y pueden salvar la vida de las aves.",
                'sku'             =>'CASCPTSFMTAL',
                'precio_venta'    =>25.00,
                'impuestos'       =>3.75,
                'ubicacion'       =>'A4',
                'imagen'          =>'/veterinaria/images/articulos/cscabelMetal.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>5, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 6=MININO 7=Cat´s Best
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
            [                        //-------------------------------------- 
                'nombre'          =>"Cat Grass de Trigo", //30
                'descripcion'     =>"Cat Grass de Un Dos Treats es pasto de trigo para gatos que proporciona proteína y nutrición a tu gato y otras mascotas. La mayoría de las mascotas se sienten atraídos al pasto y comerán algunas plantas del exterior.",
                'sku'             =>'PREMPTSFMTAL',
                'precio_venta'    =>86.00,
                'impuestos'       =>12.9,
                'ubicacion'       =>'A4',
                'imagen'          =>'/veterinaria/images/articulos/pastoTrigo.jpg',
                'tipoUnidad'      =>'PZA', //30
                'unidadMax'       =>5,
                'existencias'     =>4,
                'estatus'         =>1, 
                'id_empleado'     =>1, //1 = HUGO ALEJANDRO
                'id_marca'        =>8, //1=DOGLINE 2=NERFDOG 3= DIAMOND 4=PETSTAR 5=PET SAFE 6=MININO 7=Cat´s Best 8=Un Dos Treat
                'id_presentacion' =>1, //1=PZA 2=PAQUETE 3=BOLSA
                'id_departamento' =>1, //1= ARTICULOS 2=MEDICAMENTOS 3=SERVICIOS
            ],
             
        );

        /*Insertar el array a travez del modelo*/ 
        Product::insert($data);
    }//.run
}//.ProductTableSeeder