<?php
/*
|--------------------------------------------------------------------------
| Seeder Puestos
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class cat_puestos_Seeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cat_puestos')->insert(
            [
            'nombre' => 'Ayudante General',
            'estatus' => 1,
            'id_area' => 1,
            ],
            
        );

    }//.run
}//.cat_puestos_Seeder
