<?php
/*
|--------------------------------------------------------------------------
| Seeder Usuarios
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

use Illuminate\Database\Seeder;
use App\User; //incluir nuestro modelo

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class UserTableSeeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     *
     * @return void
     */
    public function run()
    {
        //Método truncate, que se encarga de vaciar la table
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

        $data = array(
           [
                'name'       => 'Hugo',
                'apellidos'  => 'Torre',
                'domicilio'  => 'Tulipanes #1414 Col. Sauz',
                'curp'       => 'CURPTOMH',
                'rfcCliente' => 'RFCPTOMH123',
                'telefono'   => '3317852458',
                'email'      => 'hugo@gmail.com',
                'password'   => \Hash::make('12345678'),
                'created_at' => new DateTime
               
            ],
          
        );
        
        /*Insertar el array a travez del modelo*/ 
        User::insert($data);  //atravez de nuestro moedelo user insertar en la tabla lo que tenga nuestro arreglo $data
        
    }//.run

    
}//.UserTableSeeder
