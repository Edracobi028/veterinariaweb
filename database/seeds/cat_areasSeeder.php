<?php
/*
|--------------------------------------------------------------------------
| Seeder Áreas
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

/**
 * @desc Clase donde podemos insertar datos a las tablas de la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class cat_areasSeeder extends Seeder
{
    /**
     * Método para insertar datos en labase de datos.
     * @return void
     */
    public function run()
    {
        
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;'); // Desactivamos la revisión de claves foráneas
        DB::table('cat_areas')->truncate();           // utilizar el método truncate, que se encarga de vaciar la table
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;'); // Reactivamos la revisión de claves foráneas

        /* crear un array para insertar informacion a una tabla */
        $data = array(
        
            [
                'nombre'     => 'Ventas',
                'estatus'    => 1,
            ],
            [
                'nombre'     => 'Administrador',
                'estatus'    => 1,    
            ],
            [
                'nombre'     => 'Almacenes',
                'estatus'    => 1,    
            ]
            
        );

        /*Insertar el array a travez del modelo*/  
        DB::table('cat_areas')->insert($data);

    }//.run
}//.cat_areasSeeder
