<?php

/*
|--------------------------------------------------------------------------
| Seeder Database 
|--------------------------------------------------------------------------
| Desde aqui es donde se van a llamar los Seeders creados
*/

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * @desc Clase donde invocamos los seeders que llenaran de informacion en la base de datos,
 * puedes encontrar metodos como run().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Método para llamar las clases seeders que se insertarán datos en la base de datos.
     * @return void
     */
    public function run()
    {
       
        $this->call(UserTableSeeder::class);
        //$this->call(CategoryTableSeeder::class);
        $this->call(cat_areasSeeder::class);
        $this->call(cat_departamentos_Seeder::class);
        $this->call(cat_presentaciones_Seeder::class);
        $this->call(cat_puestos_Seeder::class);
        $this->call(empleados_Seeder::class);
        $this->call(cat_marcas_Seeder::class);
        $this->call(ProductTableSeeder::class);

    }//.run
}//.DatabaseSeeder
