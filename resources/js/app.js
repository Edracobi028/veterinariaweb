/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//Donde se registran todos los componentes

require('./bootstrap');
window.$ = window.jQuery = require('jquery');
window.Vue = require('vue');

/**
 * 
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//1.Crear
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pedidos', require('./components/Pedidos.vue'));


//2. Importas
import pedidos from './components/Pedidos';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 //3.Lo montas
 //Aqui va el campo id del html
const app = new Vue({
    el: '#app',
    data: {//aqui se mandan parametros
        menu:0     
    },
    components:{
        pedidos,
    }
});
