<!---
|--------------------------------------------------------------------------------
| HomeVeterinaria
|--------------------------------------------------------------------------------
| Archivo html que muestra el contenido de la pagina de inicio de Veterinaria el Paraiso
-->

<!-- Herencia de Header -->
@extends('layouts.default')

@section('scroll', true)

<!-- Titulo de página -->
@section('titulo', 'El Paraíso | Veterinaria')

<!--Contenido para sobrescribir -->
@section('content')


    <section id="inicio">
        <div class="callbacks_container">
            <ul class="rslides" id="slider3">
                <li class="banner banner1">
                    <div class="container">
                        <div class="banner-text">
                            <div class="slider-info">
                                <h3>El paraíso</h3>
                                <span class="line"></span>
                                <p>Tu veterinaria</p>
                                <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll" data-blast="bgColor" href="#about"
                                    role="button">Ver
                                    Más</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="banner banner2">
                    <div class="container">
                        <div class="banner-text">
                            <div class="slider-info">
                                <h3>Adopta un amigo</h3>
                                <span class="line"></span>
                                <p>Ayúdanos a encontrarles un hogar</p>
                                <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll" data-blast="bgColor" href="#adopta"
                                    role="button">Ver
                                    Más</a>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="banner banner3">
                    <div class="container">
                        <div class="banner-text">
                            <div class="slider-info">
                                <h3>Descubriendo creaturas</h3>
                                <span class="line"></span>
                                <p>Movemos el mundo para cuidar tu mascota</p>
                                <a class="btn bg-theme mt-4 w3_pvt-link-bnr scroll" data-blast="bgColor" href="#about"
                                    role="button">Ver
                                    Más</a>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    </section>
    <!-- //banner -->
    <!--  about -->
    <section class="wthree-slide-btm pt-lg-5" id="about">
        <div class="container pt-sm-5 pt-4">
            <div class="row no-gutters">
                <div class="col-lg-5 wow bounceInLeft" data-wow-duration="3s">
                    <div class="slide-banner"></div>
                </div>
                <div class="col-lg-7 wow bounceInUp" data-wow-duration="3s">
                    <div class="bg-abt light-bg">
                        <div class="container">
                            <div class="title-desc  pb-sm-3">
                                <h3 class="main-title-w3pvt text-center">¿Quienes somos?</h3>
                            </div>
                            <div class="row flex-column mt-lg-4 mt-3">
                                <div class="abt-grid mb-4">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="abt-icon" data-blast="bgColor">
                                                <span class="fa fa-home"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <h4>Misión</h4>
                                                <p class="text-justify text-description">Proporcionar servicios Médico Veterinario de calidad con ética 
                                                    y responsabilidad profesional para nuestros pacientes y clientes.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="abt-grid mt-4">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="abt-icon" data-blast="bgColor">
                                                <span class="fa fa-stethoscope"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <h4>Visión</h4>
                                                <p class="text-justify text-description">
                                                    Capacitación y actualización continua y especializada haciendo uso de la herramientas para 
                                                    la solución de los problemas en medicina, cirugía y salud de las mascotas.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //about -->
    <!--  about bottom -->
    <section class="wthree-slide-btm pb-lg-5" id="adopta">
        <div class="container pb-md-5 pb-4">
            <div class="row flex-row-reverse no-gutters">
                <div class="col-lg-5 wow bounceInLeft" data-wow-duration="3s">
                    <div class="ab-banner">
                    </div>
                </div>
                <div class="col-lg-7 wow bounceInLeft" data-wow-duration="3s">
                    <div class="bg-abt">
                        <div class="container">
                            <div class="title-desc  pb-sm-3">
                                <h3 class="main-title-w3pvt text-center">Adopta un amigo</h3>
                            </div>
                            <div class="row flex-column mt-lg-4 mt-3">
                                <div class="abt-grid mb-4">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="abt-icon" data-blast="bgColor">
                                                <span class="fa fa-heart"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <h4>Adopta un amigo </h4>
                                                <p class="text-justify text-description"> Es un programa en el cual
                                                    rescatamos animales en situación de calle y los ayudamos a encontrar un hogar, 
                                                    Ven, únete e invita a tus amigos a nuestro programa y ayúdanos.
                                                    Ellos te lo agradecerán.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="abt-grid mt-4 pt-lg-2">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="abt-icon" data-blast="bgColor">
                                                <span class="fa fa-exclamation"></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-9 pl-sm-0">
                                            <div class="abt-txt ml-sm-0">
                                                <h4>Visítanos</h4>
                                                <p class="text-justify text-description">
                                                    Ven y adopta a uno de nuestros amigos y hazlo tu compañero de vida.
                                                    Amor, fidelidad, risas y felicidad es lo que te brindará este amigo que te espera.
                                                </p>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //about boottom -->
    <!-- stats -->
    <section class="w3_stats py-sm-5 py-4" id="stats" >
        <div class="container wow zoomIn" data-wow-duration="3s">
        <div class="title-desc text-center pull-center pb-sm-3">
            <div class="py-lg-5 w3-stats">
                <h2 class="w3pvt-title">Estadísticas
                </h2>
                <h4 class="w3pvt-title title-desc text-white">
                                            Record de consultas y usuarios registrados</h4>
                <div class="row py-4">
                    <div class="col-md-4 col-6">
                        <div class="counter">
                            <span class="fa fa-smile-o"></span>
                            <div class="timer count-title count-number mt-2 text-white" data-to="297" data-speed="1500"></div>
                            <h4 class="count-text text-uppercase text-white">Mascotas atendidas</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-6">
                        <div class="counter">
                            <span class="fa fa-users"></span>
                            <div class="timer count-title count-number mt-2 text-white" data-to="194" data-speed="1500"></div>
                            <h4 class="count-text text-uppercase text-white">Usuarios registrados</h4>
                        </div>
                    </div>
                    <div class="col-md-4 col-6 mt-md-0 mt-4">
                        <div class="counter">
                            <span class="fa fa-paw"></span>
                            <div class="timer count-title count-number mt-2 text-white" data-to="63" data-speed="1500"></div>
                            <h4 class="count-text text-uppercase text-white">Mascotas adoptadas</h4>
                        </div>
                    </div>
                </div>

            </div>
</div>
        </div>
    </section>
    <!-- //stats -->
    <!-- services -->
    <div class="w3lspvt-about py-md-5 py-5 wow zoomInUp"  data-wow-duration="3s" id="services">
        <div class="container pt-lg-5">
            <div class="title-desc text-center pb-sm-3">
                <h3 class="main-title-w3pvt tittle-blue">SERVICIOS</h3>
                <p class="subtittle-blue">Pregunta por cada uno de nuestros servicios</p>
            </div>
            <div class="w3lspvt-about-row row  text-center pt-md-0 pt-5 mt-lg-5">
                <div class="col-lg-4 col-sm-6 w3lspvt-about-grids">
                    <div class="p-md-5 p-sm-3">
                        <span class="fa fa-plus-square" data-blast="borderColor"></span>
                        <h3 class="mt-2 mb-3" data-blast="color">Cirugías</h3>
                        <p class="text-center text-description-2">Atendemos a tu mascota con la mayor atención, para que así su recuperación sea pronta.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 w3lspvt-about-grids  border-left border-right my-sm-0 my-5">
                    <div class="p-md-5 p-sm-3">
                        <span class="fa fa-eyedropper" data-blast="borderColor"></span>
                        <h3 class="mt-2 mb-3" data-blast="color">Medicina interna</h3>
                        <p class="text-center text-description-2">Encuentra el medicamento adecuado para tu mascota.</p>
                    </div>
                </div>
                <div class="col-lg-4 w3lspvt-about-grids">
                    <div class="p-md-5 p-sm-3">
                        <span class="fa fa-paw" data-blast="borderColor"></span>
                        <h3 class="mt-2 mb-3" data-blast="color">Adopta un amigo</h3>
                        <p class="text-center text-description-2">Únete a nuestro programa y adopta una mascota.</p>
                    </div>
                </div>
            </div>
            <div class="w3lspvt-about-row border-top row text-center pt-md-0 pt-5 mt-md-0 mt-5">
                <div class="col-lg-4 col-sm-6 w3lspvt-about-grids">
                    <div class="p-md-5 p-sm-3 col-label">
                        <span class="fa fa-flask" data-blast="borderColor"></span>
                        <h3 class="mt-2 mb-3" data-blast="color">Laboratorio</h3>
                        <p class="text-center text-description-2">Contamos con un laboratorio completo para tratar a tu mascota satisfactoriamente.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 w3lspvt-about-grids mt-lg-0 mt-md-3 border-left border-right pt-sm-0 pt-5">
                    <div class="p-md-5 p-sm-3 col-label">
                        <span class="fa fa-star" data-blast="borderColor">
                        </span>
                        <h3 class="mt-2 mb-3" data-blast="color">Estética</h3>
                        <p class="text-center text-description-2">Trae a tu mascota y dale un servicio de 5 estrellas con un corte de pelo y un baño refrescante.</p>
                    </div>
                </div>
                <div class="col-lg-4 w3lspvt-about-grids pt-md-0 pt-5">
                    <div class="p-md-5 p-sm-3 col-label">
                        <span class="fa fa-paw" data-blast="borderColor"></span>
                        <h3 class="mt-2 mb-3" data-blast="color">Productos</h3>
                        <p class="text-center text-description-2">Ven a nuestras instalaciones y adquiere productos para el cuidado de tu mascota.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //services -->
    <!-- slide -->
    <div class="abt_bottom py-lg-5 bg-theme" data-blast="bgColor">
        <div class="container py-sm-4 py-3 wow fadeInUp"  data-wow-duration="4s">
            <h4 class="abt-text text-capitalize text-sm-center">En Veterinaria El Paraíso, Tus Consultas Son Gratuitas. 

                </h4>
                <h3 class="abt-text text-capitalize text-sm-center subtittle-blank">Llama y agenda tu cita</h3>
            <div class="d-sm-flex justify-content-center">
                <a class="btn light-bg mt-4 w3_pvt-link-bnr scroll bg-theme1" href="#contact" role="button">contacto
                    
                </a>

            </div>
        </div>
    </div>
    <!-- //slide -->
    <!-- pricing plans -->
    <section class="py-lg-5 py-4" id="plans">
        <div class="container py-md-5">
            <div class="title-desc text-center pb-sm-3 wow bounce" data-wow-duration="4s">
                <h3 class="main-title-w3pvt tittle-blue">Promociones Recientes</h3>
                <p class="subtittle-blue">Veterinaria El Paraíso</p>
            </div>
            <div class="row price-row">
                <div class="col-lg-4 col-sm-6 column mb-lg-0 mb-4 wow fadeInLeft" data-row-duration="4s">
                    <div class="box" data-blast="borderColor">
                        <div class="title">
                            <span class="fa fa-gg" data-blast="color"></span>
                            <h5 class="tittle-promociones" data-blast="color">Consultas</h5>
                        </div>
                        <div class="price">
                            <h6>Consultas - <sup></sup><span class="font-weight-bold">Gratuitas</span></h6>
                        </div>
                        <div class="option">
                            <ul>
                                <li>revisión</li>
                                <li>0</li>
                                <li>0</li>
                                <li>0</li>
                            </ul>
                        </div>
                        <button type="button" class="btn w3ls-btn bg-theme  d-block" data-toggle="modal" aria-pressed="false"
                            data-target="#exampleModal" data-blast="bgColor">
                            Hacer cita
                        </button>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 column mb-lg-0 mb-4 wow fadeInDown" data-row-duration="4s">
                    <div class="box" data-blast="borderColor">
                        <div class="title">
                            <span class="fa fa-gg" data-blast="color"></span>
                            <h5 class="tittle-promociones" data-blast="color">Estética</h5>
                        </div>
                        <div class="price">
                            <h6>servicio - <sup>$</sup><span class="font-weight-bold">250</span></h6>
                        </div>
                        <div class="option">
                            <ul>
                                <li>Recorte de pelo</li>
                                <li>Baño</li>
                                <li>Recorte de uñas</li>
                                <li>Aroma anti-pulgas</li>
                            </ul>
                        </div>
                        <button type="button" class="btn w3ls-btn bg-theme  d-block" data-toggle="modal" aria-pressed="false"
                            data-target="#exampleModal" data-blast="bgColor">
                            Hacer cita
                        </button>
                    </div>
                </div>

                <div class="col-lg-4 col-sm-6  mx-auto mt-lg-0 mt-4 column wow fadeInRightBig" data-row-duration="4s">
                    <div class="box" data-blast="borderColor">
                        <div class="title">
                            <span class="fa fa-gg" data-blast="color"></span>
                            <h5 class="tittle-promociones" data-blast="color">Kit paseo</h5>
                        </div>
                        <div class="price">
                            <h6>Kit paseo - <sup>$</sup><span class="font-weight-bold">400</span></h6>
                        </div>
                        <div class="option">
                            <ul>
                                <li>Correa</li>
                                <li>Collar</li>
                                <li>Placa con nombre</li>
                                <li>Pechera</li>
                            </ul>
                        </div>
                        <button type="button" class="btn w3ls-btn bg-theme  d-block" data-toggle="modal" aria-pressed="false"
                            data-target="#exampleModal" data-blast="bgColor">
                            Comprar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- //pricing plans -->
    <!-- team -->
    
    <!-- //team -->
    <!-- portfolio 
    <section class="wthree-row w3-gallery cliptop-portfolio-wthree pt-lg-5" id="portfolio">
        <div class="container-fluid">
            <div class="title-desc text-center pb-3">
                <h3 class="main-title-w3pvt">Portafolio</h3>
                <p>Veterinaria El Paraíso</p>
            </div>
            <ul class="demo row py-lg-5 py-sm-4 pb-4">
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y6.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y1.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y14.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y15.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y11.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y4.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y10.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y8.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y2.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y18.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6">
                    <div class="gallery-grid1">
                        <img src="leon/images/y16.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
                <li class="col-lg-3 col-sm-6 ">
                    <div class="gallery-grid1 ">
                        <img src="leon/images/y17.jpg" alt=" " class="img-fluid img-thumbnail" />
                    </div>
                </li>
               
               
            </ul>
        </div>
    </section>-->
    <!-- //portfolio -->




   
    <!-- contact -->
    <section class="contact-wthree py-sm-5 py-4" id="contact">
        <div class="container pt-lg-5">
            <div class="title-desc text-center pb-sm-3">
                <h3 class="main-title-w3pvt tittle-blue">Contacto</h3>
                <p class="subtittle-blue">Veterinaria El Paraíso</p>
            </div>
            <div class="row mt-4">
                <div class="col-lg-5 text-center">
                    
                    <div class="row flex-column">
                        <div class="contact-w3">
                            <span class="fa fa-envelope-open  mb-3" data-blast="color"></span>
                            <div class="d-flex flex-column">
                                <a href="mailto:example@email.com" class="d-block">contacto@elparaiso.com</a>
                             
                            </div>
                        </div>
                        <div class="contact-w3 my-4">
                            <span class="fa fa-phone mb-3" data-blast="color"></span>
                            <div class="d-flex flex-column">
                                <p class="item-contact">4040 9759</p>
                                <p class="item-contact my-1">+52 1 3334938733</p>
                               
                            </div>
                        </div>
                        <div class="contact-w3">
                            <span class="fa fa-home mb-3" data-blast="color"></span>
                            <p class="item-contact">Mar # 101 <br>Esquina Av. El Paraíso<br>Fraccionamiento El Paraiso, Tlajomulco De Zúñiga, Jal.</p>
                        </div>
                    </div>

                </div>
                <div class="col-lg-7">
                    
                    <div class="contact-form-wthreelayouts">
                        <form action="#" method="post" class="register-wthree mt-4">
                            <div class="form-group pt-4">
                                <label class="label-contact">
                                    Tu Nombre
                                </label>
                                <input class="form-control" type="text" placeholder="Alejandro" name="name" required="">
                            </div>
                            <div class="form-group pt-4">
                                <label class="label-contact">
                                    Teléfono
                                </label>
                                <input class="form-control" type="text" placeholder="xxxx xxxxx" name="tel" required="">
                            </div>
                            <div class="form-group pt-4">
                                <label class="label-contact">
                                    Email
                                </label>
                                <input class="form-control" type="email" placeholder="ejemplo@hotmail.com" name="email"
                                    required="">
                            </div>
                            <div class="form-group pt-4">
                                <label class="label-contact">
                                    Tu Mensaje
                                </label>
                                <textarea placeholder="Ingresa Tu Mensaje Aqui" class="form-control"></textarea>
                            </div>
                            <div class="form-group pt-4 mb-0">
                                <button type="submit" class="btn btn-w3layouts btn-block  bg-theme text-white w-100 font-weight-bold text-uppercase"
                                    data-blast="bgColor">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="mx-auto text-center mt-lg-0 mt-4">
            <iframe class="mt-lg-4 contact-form-wthreelayouts" data-blast="borderColor" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3734.293477171682!2d-103.35790448578358!3d20.616891186222976!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8428b2808dcece3d%3A0xc674958996b8b184!2sUniversidad+Tecnol%C3%B3gica+de+Jalisco!5e0!3m2!1ses!2smx!4v1565906129172!5m2!1ses!2smx" allowfullscreen></iframe>
               
                <!-- //footer right -->
            </div>
        </div>

    </section>
    <!-- //contact -->
    
@endsection
   
