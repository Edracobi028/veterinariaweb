<!---
|--------------------------------------------------------------------------------
| Index
|--------------------------------------------------------------------------------
| Pagina html que contendrá nuestro catálogo
-->

<!-- Herencia de Header -->
@extends('layouts.default')

<!-- Titulo de página -->
@section('titulo', 'Artículos | Veterinaria')

<!--Contenido para sobrescribir -->
@section('content')

<!-- Herencia de Slider -->
@include('store.partials.sliderStore')
<section id="articulos mt-4">
    <div class="container text-center styleCatalogo">
        <div id="products" class="mt-4" >
            <!-- Ciclo muestra productos  -->
            @foreach($products as $product)
                <div class="product white-panel">
                    <h3 class="precio-text">{{ $product->nombre }}</h3><hr>
                    <img src="{{ $product->imagen }}" width='250'>
                    <div class="product-info panel">
                        <!--<p>{{ $product->descripcion }}</p>-->
                        <div class="row justify-content-center">
                            <h3 class="precio-text mt-3">Precio: ${{ number_format($product->precio_venta,2) }}</h3>
                        </div>
                        <div class="row mt-4">
                            <div class="col-md-12">
                                <a class="btn btn-warning" href="{{ route('cart-add', $product->sku) }}">
                                    <i class="fa fa-cart-plus"></i> Agregar
                                </a>
                            </div>
                            <div class="col-md-12 mt-3">
                            <a class="btn btn-primary" href="{{ route('product-detail', $product->sku) }}"><i class="fa fa-chevron-circle-right"></i> Leer más</a>
                            </div>
                            
                        </div>

                    </div>
                </div>
            @endforeach
            
        </div>
        
    </div>
</section>    
@stop
<!-- //.Contenido para sobrescribir -->
