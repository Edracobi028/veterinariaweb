<!---
|--------------------------------------------------------------------------------
| Cart
|--------------------------------------------------------------------------------
| Pagina html donde muestra los articulos contenidos en el carrito de compras
-->

<!-- Herencia de Header -->
@extends ('layouts.default')

<!--Contenido para sobrescribir -->
@section ('content')
    
    <div id="fondo_barra_nav" >
        <br>
        <br>
        <br>
        <br>
    </div>
    <div class="container text-center">
        <div class="page-header">
            <br>
            <h1 class="tittle-description-detail"><i class="fa fa-shopping-cart"></i> Carrito de compras</h1>
        </div><hr>
      <div class="table-cart">
            @if(count($cart))     
                <p>
                    <a href="{{ route('cart-trash') }}" class="btn btn-danger">
                        Vaciar carrito <i class="fa fa-trash"></i>
                    </a>
                </p>
                <br>     
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <tr>
                                <th class="th-pedido">Imagen</th>
                                <th class="th-pedido">Producto</th>
                                <th class="th-pedido">Precio</th>
                                <th class="th-pedido">Cantidad</th>
                                <th class="th-pedido">Subtotal</th>
                                <th class="th-pedido">Quitar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cart as $item)
                                <tr>
                                    <td class="th-pedido"><img src="{{  $item->imagen }}" ></td>
                                    <td class="th-pedido">{{ $item->nombre }}</td>
                                    <td class="th-pedido">${{ number_format($item->precio_venta,2) }}</td>
                                    <td class="th-pedido">
                                        <input 
                                            type="number"
                                            min="1"
                                            max="100"
                                            value="{{ $item->quantity }}"
                                            id="product_{{ $item->id }}"
                                        >
                                        <a 
                                            href = "#" 
                                            class = "btn btn-warning btn-update-item "
                                            data-href = "{{ route('cart-update', $item->sku) }}"
                                            data-id = "{{ $item->id }}"
                                        >
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </td>
                                    <td class="th-pedido">${{ number_format($item->precio_venta * $item->quantity,2) }}</td>
                                    <td class="th-pedido">
                                        <a href="{{ route('cart-delete', $item->sku) }}" class="btn btn-danger">
                                            <i class="fa fa-remove"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table><hr>
                    <h3 class="precio-text">
                        Total: ${{ number_format($total,2) }}
                    </h3>
                </div>
                @else
                    <h3><span class="label label-warning">No hay productos en el carrito :(</span></h3>
                @endif
                <hr>
                <p>
                    <a href="{{ route('index') }}"  class="btn btn-primary">
                        <i class="fa fa-chevron-circle-left"></i> Seguir comprando
                    </a>

                    <a href="{{ route('order-detail') }}" class="btn btn-primary">
                        Continuar <i class="fa fa-chevron-circle-right"></i>
                    </a>
                </p>
        </div>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
    </div>
@stop
<!-- //.Contenido para sobrescribir -->

