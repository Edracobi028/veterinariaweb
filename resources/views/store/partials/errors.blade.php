<!---
|--------------------------------------------------------------------------------
| Errors
|--------------------------------------------------------------------------------
| Archivo de tipo html que muestra errores
-->
<!-- Mensajes de Errores -->
@if (count($errors) > 0) 
  <div class="alert alert-danger">
    <ul>
       @foreach ($errors->all() as $error)
           <li> {{ $error }}</li>
         @endforeach
    </ul>
 </div> 
 @endif
 <!-- //.Mensajes de Errores --> 