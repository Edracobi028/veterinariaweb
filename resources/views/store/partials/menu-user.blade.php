<!---
|--------------------------------------------------------------------------------
| Menu-user
|--------------------------------------------------------------------------------
| Archivo html que muestra links de menú del usuario registrado
-->
<!-- Condicion para mostrar opcion si el usuario esta logeado -->
@if(Auth::check())
    <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="" role="button" aria-expanded="false">
                <i class="fa fa-user"></i> <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
                <li><a href=" {{ route('logout') }} ">Finalizar sesión</a></li>
            </ul>  
    </li>
@else
    <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-expanded="false">
            <i class="fa fa-user"></i><span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href=" {{ __('Iniciar Sesión') }}">Iniciar sesión</a></li>
          </ul>  
    </li> 
@endif       
