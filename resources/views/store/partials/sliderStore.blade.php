<!---
|--------------------------------------------------------------------------------
| Slider Store
|--------------------------------------------------------------------------------
| Slider de la página de inicio
-->
<div id="slider" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#slider" data-slide-to="0" class="active"></li>
    <li data-target="#slider" data-slide-to="1"></li>
    <li data-target="#slider" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img src="/leon/images/slide_3.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="/leon/images/slide_6.jpg" class="d-block w-100" alt="...">
    </div>
    <div class="carousel-item">
      <img src="/leon/images/slide_7.jpg" class="d-block w-100" alt="...">
    </div>
  </div>

  <!-- Controls -->
  <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>

  <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
<hr>