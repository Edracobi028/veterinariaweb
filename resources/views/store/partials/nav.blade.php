<!---
|--------------------------------------------------------------------------------
| Barra de navegación
|--------------------------------------------------------------------------------
| Pagina html donde interactuamos con links y botones para navegar en home de articulos
-->

<!-- Barra de navegacion -->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand main-title" href="{{ url('home') }}">Veterinaria</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="navbar-text" href="{{ url('index') }}" >Catalogo Artículos</a>
        </li>
     
      </ul>
      <ul class=" navbar-nav dropdown ">
        <li><a class="nav-link" href="{{ route('cart-show') }}"><i class="fa fa-shopping-cart"></i></a></li>
        <li><a class="nav-link" href="/indexPedidos">Pedidos</a></li>
        <li><a class="nav-link" href="#">Contacto</a></li>
       <!-- //@include('store.partials.menu-user') -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" 
              data-toggle="dropdown" 
              href="#" 
              role="button" 
              aria-haspopup="true" 
              aria-expanded="false"
              ><i class="fa fa-user"></i><span class="caret"></span>
            </a>
            <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 37px, 0px);">
            <a class="dropdown-item " href="{{route('login')}}">Iniciar sesión</a>
            <a class="dropdown-item " href="{{route('logout')}}">Mis pedidos</a>
          </div>
        </li> 
      </ul> 
    </div>

</nav>
<!-- //.Barra de navegacion -->


