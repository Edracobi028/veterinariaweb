<!---
|--------------------------------------------------------------------------------
| Orders1
|--------------------------------------------------------------------------------
| Plantilla donde muestra pedidos del cliente
-->

<!-- Herencia de Header -->
@extends ('store.template')

<!--Contenido para sobrescribir -->
@section ('content')
<br>
<br>
<br>
<br>
Pedidos del cliente...
<div class="page">
	<div class="container text-center bloque-detalle">
		<div class="row ">
			<div class=" col-sm-12">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div class="panel-title">
							<div class="row">
								<div class="col-sm-12 titulo-detalle" >
									<h3><span class=""></span> Mis pedidos</h3>
								</div>
							
							<div class="col-sm-12">
									<div class="row">
										<div class="col-sm-3">    
									<h4 class="product-name"><strong>Id Pedido</strong></h4>
									</div>
										<div class="col-sm-3">    
									<h4 class="product-name"><strong>Fecha</strong></h4>
									</div>
								<div class="col-sm-3">    
									<h4 class="product-name"><strong>Estatus</strong></h4>
									</div>
								<div class="col-sm-3">    
									<h4 class="product-name"><strong>Total</strong></h4>
									</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					
		
					<div class="panel-body">

						<a href="#" class="row">
												
							<div class="col-sm-3">    
								<h4><small></small> 001 </h4>
							</div>
							
							<div class="col-sm-3">
								<h4><small class="fecha">16 / 08 / 2019</small></h4>
							</div>
							<div class="col-sm-3">
								<h4><small class="fecha"> Enviado </small></h4>
							</div>
							<div class="col-sm-3">
								<h4><small class="fecha">$ 485.00</small></h4>
							</div>
						
						</a>
					
						<hr>	
						
					</div>	
			
				</div>	
			</div>
		</div>
	</div>
</div>

<br>
<br>
<br>
<br>

@stop
<!-- //.Contenido para sobrescribir -->