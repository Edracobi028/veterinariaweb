<!---
|--------------------------------------------------------------------------------
| Show
|--------------------------------------------------------------------------------
| Pagina html donde mostramos el detalle del producto
-->

<!-- Hereda plantilla de header -->
@extends('layouts.default')

<!--Contenido para sobrescribir -->
@section('content')
<div id="fondo_barra_nav" >
        <br>
        <br>
        <br>
        <br>
</div>

<!-- Detalle del producto -->
<div class="container text-center " >
    <br>
    <div class="page-header">
        <h1 class="tittle-description-detail"><i class="fa fa-shopping-cart"></i> Detalle del Producto</h1>
    </div><hr>
    
    <div class="row">
        <div class="col-md-6">
            <div class="product-block">
                <img src="{{ $product->imagen}}" >
            </div>
        </div>
        <div class="col-md-6">
            <div class="product-block">
                <h3 class="tittle-description-detail">{{ $product->nombre }}</h3><hr>
                <div class="product-info panel">
                    <p class="tittle-description-detail description-article text-justify">{{ $product->descripcion}}</p>
                    <h3 class="tittle-description-detail mt-4">
                        Precio: ${{ number_format($product->precio_venta,2)}}
                    </h3>
                    <p class="mt-4">
                        <a class="btn btn-warning btn-block" href="{{ route('cart-add', $product->sku) }}"> 
                            La quiero <i class="fa fa-cart-plus fa-2x"></i>
                        </a>
                    </p>      
                </div>
            </div>
        </div>
    </div><hr>

    <p>
        <a class="btn btn-primary detalle" href="{{url('index')}}">
            <i class="fa fa-chevron-circle-left"> Regresar</i>
        </a>
    </p>
    <br>
</div>
@stop