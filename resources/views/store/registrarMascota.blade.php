<!---
|--------------------------------------------------------------------------------
| Registrar Mascota
|--------------------------------------------------------------------------------
| Archivo html que muestra formulario para registrar una o varias mascotas.
-->

<!-- Herencia de Header -->
@extends('store.template')

<!--Contenido para sobrescribir -->
@section('content')

<registrar-mascota></registrar-mascota>
@stop
<!-- //.Contenido para sobrescribir -->