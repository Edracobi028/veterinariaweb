<!---
|--------------------------------------------------------------------------------
| Template
|--------------------------------------------------------------------------------
| Plantilla que nos servirá de bae para generar todas nuestra vistas
-->

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">  <!--jsnode-->
    
    <title>@yield('title', 'Artículos')</title>
    
 
    <link rel="stylesheet" href=" {{ asset('lumen/bootstrap.min.css')}}" >  
    <link href="{{ asset('leon/css/font-awesome.min.css')}}" rel="stylesheet" > <!-- este-->
    <!-- Fuentes google -->
    <link href="https://fonts.googleapis.com/css?family=Lobster+Two|Poiret+One&display=swap" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <link rel="stylesheet" href=" {{ asset('css/mainStore.css')}}" >
    <link rel="stylesheet" href="{{leon/css/bootstrap.css}}">    
    <link rel = "stylesheet" href = " {{ asset('css/animate.css')}}">
    <link href="{{ asset('cleon/css/font-awesome.min.css')}}" rel="stylesheet">

</head>
  <body>

      <!-- Herencia de Alertas -->
      @if(\Session::has('message'))
        @include('store.partials.message')
      @endif  
      
      <!--   <div id="app">  jsnode-->
      @include('store.partials.nav')

      <!--Contenido para sobrescribir -->
      @yield('content')

      
      @include('layouts.footerHome')

      <!--  Scripts   -->
      <!--  <example-component></example-component>  jsnode-->
      <!--  </div>  jsnode-->
      <!--  <script src="js/app.js" ></script>   jsnode    nota: se comentó x que afecta el dropdown --> 
      <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>   <!--jsnode-->
      <script src="lumen/jquery-2.2.4.min.js"></script>  <!-- este-->
      <script src="lumen/bootstrap.min.js" ></script> <!-- este-->
      <script src="js/pinterest_grid.js"></script>
      <script src="js/mainStore.js"></script>
      <script src = "js/wow.min.js"> </script>
      <script>
        new WOW (). init ();
      </script>
  </body>
</html>

