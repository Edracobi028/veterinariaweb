<!---
|--------------------------------------------------------------------------------
| Detalle de pedido
|--------------------------------------------------------------------------------
| Archivo html que muestra el detalle del pedido creado por el cliente
-->

<!-- Herencia de Header -->
@extends ('layouts.default')

<!--Contenido para sobrescribir -->
@section ('content')
    <div id="fondo_barra_nav" >
        <br>
        <br>
        <br>
        <br>
    </div>
    <div class="container text-center">
        <br>
        <div class="page-header">
            <h1 class="precio-text"><i class="fa fa-shopping-cart"></i> Detalle del pedido</h1><hr>
        </div>

        <div class="page">
            <div class="table-responsive">
                <h3 class="precio-text mb-4">Datos del usuario</h3>
                <table class="table table-striped table-hover table-bordered">
                    <tr class="th-pedido"><td>Nombre</td><td>{{ Auth::user()->name . " " . Auth::user()->apellidos }}</td></tr>
                    <!-- //   <tr><td>Usuario</td><td>{{ Auth::user()->user }}</td></tr>   -->
                    <tr class="th-pedido"><td>Correo</td><td>{{ Auth::user()->email }}</td></tr>
                    <tr class="th-pedido"><td>Domicilio</td><td>{{ Auth::user()->domicilio }}</td></tr>
                </table>
            </div>
            <div class="table-responsive">
                <h3 class="precio-text mt-3 mb-4">Datos del pedido</h3>
                <table class="table table-striped table-hover table-bordered">
                    <tr>
                        <th class="th-pedido">Producto</th>
                        <th class="th-pedido">Precio</th>
                        <th class="th-pedido">Cantidad</th>
                        <th class="th-pedido">Subtotal</th>
                    </tr>
                    @foreach($cart as $item)
                        <tr>
                            <td class="th-pedido">{{ $item->nombre }}</td>
                            <td class="th-pedido">${{ number_format($item->precio_venta,2) }}</td>
                            <td class="th-pedido">{{ $item->quantity }}</td>
                            <td class="th-pedido">${{ number_format($item->precio_venta * $item->quantity,2) }}</td>
                        </tr>
                    @endforeach    
                </table><hr>
                <h3>
                    <span class="label btn-success">
                        Total: ${{  number_format($total,2)}}
                    </span>
                </h3><hr>
                <p>
                    <a href="{{ route('cart-show') }}" class="btn btn-primary">
                        <i class="fa fa-chevron-circle-left fa-2x"></i> Regresar
                    </a>

                    <a href="{{ route('payment') }}" class="btn btn-warning">
                        Pagar con <i class="fa fa-paypal fa-2x"></i>
                    </a>

                    <a href="{{ route('paymentEfectivo') }}" class="btn btn-success">
                        Pagar en efectivo <i class="fa fa-money fa-2x"></i>
                    </a>
                </p>
            </div>
        </div>
    </div>
@stop
<!-- //.Contenido para sobrescribir -->