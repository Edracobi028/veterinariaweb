<!--
|--------------------------------------------------------------------------------
| Default
|--------------------------------------------------------------------------------
| Archivo html de plantilla header donde se colocan links, scripts etc.
-->
<!DOCTYPE html>
<html lang="es">
    
    <!-- Head -->
    <head>
        <meta charset="utf-8" >
        <meta name="viewport" content="width=device-width, initial-scale=.9">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="keywords" content="Creature Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
        SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />

        <title>@yield('titulo') </title>
        <!--  ====  HOJAS DE ESTILO ==== -->
        <link href="{{ asset('lumen/bootstrap.min.css')}}" rel="stylesheet" > <!-- este-->
        <!-- iconos -->
        <link href="{{ asset('leon/css/font-awesome.min.css')}}" rel="stylesheet" > <!-- este-->
        <!-- bootstrap -->
        <link href="{{ asset('leon/css/bootstrap.css')}}" type="text/css" rel="stylesheet" media="all">  
        <!-- color switch -->
        <link href="{{ asset('leon/css/blast.min.css')}}" rel="stylesheet" />
        <!-- portafolio -->
        <link href="{{ asset('leon/css/portfolio.css')}}" type="text/css" rel="stylesheet" media="all">
        <link href="{{ asset('veterinaria/css/style.css')}}" type="text/css" rel="stylesheet" media="all">
        <link href="{{ asset('leon/css/style.css')}}" type="text/css" rel="stylesheet" media="all">
        <!-- iconos font-awesome-->
        <link href="{{ asset('leon/css/font-awesome.min.css')}}" rel="stylesheet">
        <!-- icono de pagina -->
        <link  rel="icon"   href="/leon/images/icono2.ico" type="image/png" >
        <!-- aspecto pinterest -->   
        <link rel="stylesheet" href=" {{ asset('admin/css/main.css')}}" >
        <link rel="stylesheet" href=" {{ asset('css/mainStore.css')}}" >     
        <link rel ="stylesheet"  href = "{{ asset('css/animate.css')}}">
        
        <!-- Fuentes google -->
        <!--<link href="https://fonts.googleapis.com/css?family=Lobster+Two|Poiret+One&display=swap" rel="stylesheet">-->
        <link rel="stylesheet" href="{{asset('css/lobster_font.css')}}">
        <!--<link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">-->
        <link rel="stylesheet" href="{{asset('css/poppins.css')}}">
        <!--<link href="//fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i" rel="stylesheet">-->
        <link rel="stylesheet" href="{{asset('css/Lato_font.css')}}">

        
        <script>
            addEventListener("load", function () {
                setTimeout(hideURLbar, 0);
            }, false);

            function hideURLbar() {
                window.scrollTo(0, 1);
            }
        </script>
    </head>
    <!-- //.Head -->
        
<!-- Body -->
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!--image onload-->
    <div class="loader"></div>

    <!-- barra de mensajes -->
      


    @include('layouts.navHome', ["scroll" => !isset($scrollLinks)])
    @if(\Session::has('message'))
        @include('store.partials.message')
    @endif  

    @yield('content')

    @include('layouts.footerHome')

    

    <!-- login  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Iniciar sesión</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                  
                  <!-- form -->
                  <form method="POST" action="{{ route('login') }}" class="p-sm-3">
                       @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">{{ __('Correo electrónico') }}</label>
                           
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                           
                        </div>
                   
                        
                        
                        <div class="form-group">
                            <label for="password" class="col-form-label">{{ __('Contraseña') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        
                        <!--Hasta Aqui todo bien -->
                        <div class="right-w3l">
                            <input type="submit" class="form-control" value="Iniciar sesión">
                        </div>
                        <div class="row sub-w3l my-3">
                            <div class="col-sm-6 sub-w3_pvt">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="brand1">
                                    <span></span>{{ __('Recordarme') }}</label>
                            </div>
                            <div class="col-sm-6 forgot-w3l text-sm-right">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('¿Olvidaste la contraseña?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <p class="text-center dont-do modal-footer1">¿No tiene una cuenta?
                            <a href="#" data-toggle="modal" data-target="#exampleModal1" class="font-weight-bold"
                                data-blast="color"> 
                                Cree una</a>

                        </p>
                    </form>
                  
                </div>
            </div>
        </div>
    </div>
    <!-- //login -->

    <!-- register -->
    <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel1">{{ __('Registrar') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('register') }}" class="p-sm-3">
                       @csrf
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">{{ __('Nombre') }}</label>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-apellidos" class="col-form-label">{{ __('Apellidos') }}</label>
                            <input id="apellidos" type="text" class="form-control @error('apellidos') is-invalid @enderror" name="apellidos" value="{{ old('apellidos') }}" required autocomplete="apellidos" autofocus>

                                @error('apellidos')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-domicilio" class="col-form-label">{{ __('Domicilio') }}</label>
                            <input id="domicilio" type="text" class="form-control @error('domicilio') is-invalid @enderror" name="domicilio" value="{{ old('domicilio') }}" required autocomplete="domicilio" autofocus>

                                @error('domicilio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-curp" class="col-form-label">{{ __('CURP') }}</label>
                            <input id="curp" type="text" class="form-control @error('curp') is-invalid @enderror" name="curp" value="{{ old('curp') }}" required autocomplete="curp" autofocus>

                                @error('curp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-rfcCliente" class="col-form-label">{{ __('RFC') }}</label>
                            <input id="rfcCliente" type="text" class="form-control @error('rfcCliente') is-invalid @enderror" name="rfcCliente" value="{{ old('rfcCliente') }}" required autocomplete="rfcCliente" autofocus>

                                @error('rfcCliente')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>

                        <div class="form-group">
                            <label for="recipient-telefono" class="col-form-label">{{ __('Teléfono') }}</label>
                            <input id="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" name="telefono" value="{{ old('telefono') }}" required autocomplete="telefono" autofocus>

                                @error('telefono')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        
                        <div class="form-group">
                            <label for="recipient-email" class="col-form-label">{{ __('Correo electrónico') }}</label>
                            <input id="email2" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="password1" class="col-form-label">{{ __('Contraseña') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="form-group">
                            <label for="password2" class="col-form-label">{{ __('Confirma contraseña') }}</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                        <div class="sub-w3l">
                            <div class="sub-w3_pvt">
                                <input type="checkbox" id="brand2" value="">
                                <label for="brand2" class="mb-3">
                                    <span>Yo acepto los Términos y Condiciones</span>
                                </label>
                            </div>
                        </div>
                        <div class="right-w3l">
                            <input type="submit" class="form-control" value="Registrar">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- // register -->
    <!-- blog modal1 -->
    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel2"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-theme">
                    <h5 class="modal-title" id="exampleModalLabel2">Cras ultricies ligula sed.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="#" class="img-fluid" alt="" />
                    <p class="text-left my-4">
                        Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet
                        nisl
                        tempus convallis quis ac
                        lectus. Cras ultricies ligula sed magna dictum porta.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- //blog modal1 -->
    <!-- blog modal2 -->
    <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-theme">
                    <h5 class="modal-title" id="exampleModalLabel3">Cras ultricies ligula sed.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="#" class="img-fluid" alt="" />
                    <p class="text-left my-4">
                        Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet
                        nisl
                        tempus convallis quis ac
                        lectus. Cras ultricies ligula sed magna dictum porta.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- //blog modal2 -->
    <!-- blog modal3 -->
    <div class="modal fade" id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel4"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header bg-theme">
                    <h5 class="modal-title" id="exampleModalLabel4">Cras ultricies ligula sed magna.</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <img src="#" class="img-fluid" alt="" />
                    <p class="text-left my-4">
                        Quisque velit nisi, pretium ut lacinia in, elementum id enim. Curabitur non nulla sit amet
                        nisl
                        tempus convallis quis ac
                        lectus. Cras ultricies ligula sed magna dictum porta.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- //blog modal3-->


    <!--  ====  ZONA DE SCRIPTS ====  -->
    <!-- js -->
    
    <!-- aspecto pinterest -->
    <!--  <example-component></example-component>  jsnode-->
    <!--  </div>  jsnode-->
    <!--  <script src="js/app.js" ></script>   jsnode    nota: se comentó x que afecta el dropdown --> 
    <!--<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>   jsnode-->
    <script src="{{ asset('lumen/jquery-2.2.4.min.js')}}"></script>  <!-- este-->
    <script src="{{ asset('lumen/bootstrap.min.js')}}" ></script> <!-- este-->
    
    <script src="{{ asset('js/mainStore.js')}}"> </script>
    <script src = "{{ asset('js/wow.js')}}"> </script>
    <script>
        new WOW (). init ();
    </script>

    <!--<script src="{{ asset('leon/js/jquery-2.2.3.min.js')}}"></script> se comenta por que ya existe jquery lineas arriba-->
    <script src="{{ asset('leon/js/bootstrap.js')}}"></script>
    <!-- //js -->

    <!-- Banner Slider -->
	<script src="{{ asset('leon/js/responsiveslides.min.js')}}"></script>
	<script>
		$(function () {
			$("#slider3").responsiveSlides({
				auto: true,
				pager: true,
				nav: true,
				speed: 1000,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});
		});
    </script>
    <!--<script src="{{ asset('leon/js/jquery-2.2.3.min.js')}}"></script> ya existe jquery en lineas anteriores-->
	<!-- //Banner Slider -->
    <!-- script for password match -->
    <script>
        //window.onload = function () {
        //    document.getElementById("password1").onchange = validatePassword;
        //    document.getElementById("password2").onchange = validatePassword;
        //}

        function validatePassword() {
            var pass2 = document.getElementById("password2").value;
            var pass1 = document.getElementById("password1").value;
            if (pass1 != pass2)
                document.getElementById("password2").setCustomValidity("Passwords Don't Match");
            else
                document.getElementById("password2").setCustomValidity('');
            //empty string means no validation error
        }
    </script>
    <!-- script for password match -->
    <!-- Banner  Responsiveslides -->
    <script src="{{ asset('leon/js/responsiveslides.min.js')}}"></script>
    
    <script>
        // You can also use"$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider3").responsiveSlides({
                auto: true,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!-- //Banner  Responsiveslides -->

    <!-- Scrolling Nav JavaScript -->
    <script src="{{ asset('leon/js/scrolling-nav.js')}}"></script>
    <script src="{{ asset('leon/js/counter.js')}}"></script>

    <!-- portfolio -->
    <script src="{{ asset('leon/js/jquery.picEyes.js')}}"></script>
    <script>
        $(function () {
            //picturesEyes($('.demo li'));
            $('.demo li').picEyes();
        });
    </script>

    <!-- //portfolio -->
    <!-- start-smooth-scrolling -->
    <script src="{{ asset('leon/js/move-top.js')}}"></script>
    <script src="{{ asset('leon/js/easing.js')}}"></script>

    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                if($(this).data("scroll")){
                    event.preventDefault();

                    $('html,body').animate({
                        scrollTop: $(this.hash).offset().top
                    }, 1000);
                }
            
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->

  <!-- smooth-scrolling-of-move-up -->
	<script>
		$(document).ready(function () {
			
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
            };
			 
			$().UItoTop({
				easingType: 'easeOutQuart'
            });

        });
    </script>
    

    <!--image onload-->
    <script type="text/javascript">
        $(window).load(function() {
            $(".loader").fadeOut("slow");
        });
    </script>
    


    <!--<script src="{{ asset('leon/js/SmoothScroll.min.js')}}"></script>
     //smooth-scrolling-of-move-up -->

    <!-- color switch -->
    <script src="{{ asset('leon/js/blast.min.js')}}"></script>
    <script src="{{ asset('js/pinterest_grid.js')}}"></script>

    <!-- Bootstrap core JavaScript -->
    <!-- 4.0 Placed at the end of the document so the pages load faster
    <script src="{{ asset('leon/js/bootstrap.js')}}"></script> -->
   
</body>
<!-- //.Body -->

</html>

