<!--
|--------------------------------------------------------------------------------
| NavHome
|--------------------------------------------------------------------------------
| Página html funciona como barra de navegacion donde encuentras links
-->
<!-- banner -->
        <div id="home">

        <!-- Esquema de colores -->
        <div class="blast-box">
            <div class="blast-frame">
                <p>color schemes</p>
                <div class="blast-colors d-flex justify-content-center">
                    <div class="blast-color">#23d48f</div>
                    <div class="blast-color">#d3b800</div>
                    <div class="blast-color">#18e7d3</div>
                    <div class="blast-color">#e5902a</div>
                    <div class="blast-color">#16d9e9</div>
                    <!-- you can add more colors here -->
                </div>
                <p class="blast-custom-colors">Choose Custom color</p>
                <input type="color" name="blastCustomColor" value="#d3b800">

            </div>
            <div class="blast-icon"><span class="fa fa-paint-brush" aria-hidden="true"></span></div>
        </div>
        <!-- //.Esquema de colorese -->

        <!-- Header -->
        <header>
            <nav class="navbar navbar-expand-lg navbar-light fixed-top navbar-expand-lg navbar-light navbar-fixed-top">
                <h1>
                    <a class="navbar-brand" href="/" data-blast="color">
                        <img src="/leon/images/logo8-1.png" >
                    </a>
                </h1>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-lg-auto text-center">
                        <li class="nav-item ">
                            <a class="nav-link scroll" data-scroll="{{ $scroll }}" href="" >Inicio
                                <span class="sr-only">(current)</span>
                            </a>
                        </li>
                        <li class="nav-item  mt-lg-0 mt-3">
                            <a class="nav-link scroll" data-scroll="{{ $scroll }}" href="/#about">Nosotros</a>
                        </li>
                        <li class="nav-item mt-lg-0 mt-3">
                            <a class="nav-link" href="{{ url('index') }}" >Artículos</a>
                        </li>
                        <li class="nav-item dropdown mt-lg-0 mt-3">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Servicios
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                               <a class="dropdown-item scroll nav-link" data-scroll="{{ $scroll }}" href="#services">Servicios</a>
                               <a class="dropdown-item nav-link" href='/citas'>Citas</a>
                               <a class="dropdown-item nav-link" href='/vacunas'>Vacunas</a>
                                <a class="dropdown-item scroll nav-link" data-scroll="{{ $scroll }}" href="#plans">Paquetes</a>
                                <a class="dropdown-item scroll nav-link" data-scroll="{{ $scroll }}" href="#team">Equipo</a>
                                <a class="dropdown-item scroll nav-link" data-scroll="{{ $scroll }}" href="#portfolio">Mascotas</a>
                                <a class="dropdown-item scroll nav-link" data-scroll="{{ $scroll }}" href="#posts">Últimos posts</a>
                            </div>
                        </li>
                        <li class="nav-item  mt-lg-0 mt-3"> <!-- -->
                            <a class="nav-link scroll" data-scroll="{{ $scroll }}" href="/#contact">Contacto</a>
                        </li>

                            <!-- login -->
                            <!-- "{{ route('login') }}" -->
                            @guest
                                <li class="nav-item ">
                                    <a class="nav-link" data-toggle="modal" data-target="#exampleModal" href=# >{{ __('Acceder') }}</a>
                                </li>
                                    @if (Route::has('register'))
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="modal" data-target="#exampleModal1" href=#>{{ __('Registrate') }}</a>
                                        </li>
                                        <button type="button" class="btn  wthree-link-bnr bg-theme rounded-circle text-center mt-lg-0 mt-3" data-toggle="modal"
                                            aria-pressed="false" data-target="#exampleModal" data-blast="bgColor"> <span class="fa fa-user text-white"></span>
                                        </button>
                                    @endif
                                @else
                                    <li class="nav-item dropdown">
                                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="/admin/orders"> Pedidos </a>
                                            <a class="dropdown-item" href="#"> Perfil </a>    
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                {{ __('Cerrar sesión') }}
                                            </a>
                                            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </li>
                                   
                                    <div class="icon-header-item">
                                        <a href="{{ route('cart-show') }}" >    
                                        <button type="button"  class="btn  wthree-link-bnr bg-theme rounded-circle text-center mt-lg-0 mt-3" data-toggle="modal"
                                            aria-pressed="false" data-target="#" data-blast="bgColor">  <span class="fa fa-shopping-cart text-white"></span>
                                        </button>
                                        </a>
                                    </div>    
                                     
                                @endguest
                                <!-- //.login -->
                    </ul>
                    <!-- boton de inico de sesion-->
                </div>
            </nav>
        </header>
        <!-- //.header -->  