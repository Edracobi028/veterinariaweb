<!--
|--------------------------------------------------------------------------------
| App
|--------------------------------------------------------------------------------
| Archivo html de plantilla header de pagina default laravel
-->

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<!-- Head original -->
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <!--<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="{{asset('css/fonts.css')}}">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!--//Head original -->

</head>
<!-- //header -->

<!-- Body -->
<body>

    <!--Contenido para sobrescribir -->
    @yield('content')


</body>
<!-- Body -->

</html>