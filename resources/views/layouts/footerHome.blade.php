 <!--
|--------------------------------------------------------------------------------
| Footer Veterinara
|--------------------------------------------------------------------------------
| Archivo html que de pie de página donde se muestran los links de comunidad de la vterinaria paraíso
-->

<!-- footer -->
 <footer class="footer-ini" dataa-blast="bgColor">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="wthree-social">
                        <ul>
                            <li>
                                <a href="#">
                                    <span class="fa fa-facebook-f icon_facebook"></span>
                                </a>
                            </li>
                           
                            
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 text-lg-left mt-lg-0 mt-4">
                    <p>© 2019 Veterinaria El paraíso. | Desarrollado por
                        <a href="#"> INI.</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    <!-- //.footer -->