<!--
|--------------------------------------------------------------------------------
| Dashboard
|--------------------------------------------------------------------------------
| Archivo html que contiene el panel de administración para el usuario con una cuenta.
-->
<!DOCTYPE html>
<html lang="en">

<!-- Head -->
<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin - Blank Page</title>
  <link rel="stylesheet" href="css/plantilla.css">
  <meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<!-- //.Head -->

<!-- Body Dashboard -->
<body id="page-top">
 
<!-- div app -->
<div id="app">

  <!-- Herencia de la barra superior del panel -->
  @include('dashboard.topbar_dashboard')

  <div id="wrapper">

  <!-- Herencia de la barra lateral del panel -->
  @include('dashboard.sidebar_dashboard')

    <!-- content-wrapper -->
    <div id="content-wrapper">

      <div class="container-fluid">

      <!-- Herencia de opciones del panel -->
      @include('dashboard.opciones_dashboard')

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- //.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>
</div>

  <!-- Scripts Dashboard-->
  <script src="js/plantilla.js"></script>
  <script src="/js/app.js"></script>
  <!-- //.Scripts Dashboard-->

</body>
<!-- //.Body Dashboard -->

</html>
