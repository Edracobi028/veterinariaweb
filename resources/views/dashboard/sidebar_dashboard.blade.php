<!--
|--------------------------------------------------------------------------------
| Sidebar_Dashboard
|--------------------------------------------------------------------------------
| Archivo de tipo html que contiene la barra lateral del panel "Dashboard"
-->    
    <!-- Sidebar Dashboard-->
    <ul class="sidebar navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="index.html">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="#">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Perifil</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" @click="menu=0" >
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Pedidos</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#" @click="menu=1">
          <i class="fas fa-fw fa-table"></i>
          <span>Vacunas</span></a>
      </li>
    </ul>
    <!-- //.Sidebar Dashboard-->