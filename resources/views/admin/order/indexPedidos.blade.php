<!--
|--------------------------------------------------------------------------------
| IndexPedidos
|--------------------------------------------------------------------------------
| Página html que funciona como panel donde muestras los pedidos de un usuario
-->

<!-- Herencia de Header -->
@extends('layouts.default')

<!--Contenido para sobrescribir -->
@section('content')

    <div id="fondo_barra_nav" >
        <br>
        <br>
        <br>
        <br>
    </div>
    <!-- Tabla que muestra pedidos -->
    <div class="container text-center">
        <br>
        <div class="page-header">
            <h1>
                <i class="fa fa-shopping-cart"></i> PEDIDOS
            </h1>
        </div><hr>

        <div class="page">

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover ">
                   <thead>
                       <tr>
                           <th>Ver detalle</th>
                           <th>Eliminar</th>
                           <th>Orden</th>
                           <th>Fecha</th>
                           <th>Usuario</th>
                           <th>Subtotal</th>
                           <th>Envio</th>
                           <th>Total</th>
                       </tr>
                   </thead> 

                   <!-- Ciclo para obtener pedidos -->
                   <tbody>
                       @foreach($orders as $order)
                       <tr>
                           <td>
                                <a 
                                        href="#"
                                        class="btn btn-primary btn-detalle-pedido"
                                        data-id="{{ $order->id }}"
                                        data-path="{{ route('admin.order.getItems') }}"
                                        data-toggle="modal"
                                        data-target="#myModal"
                                        data-token="{{ csrf_token() }}" 
                                >
                                        <i class="fa fa-external-link"></i>
                                </a>
                           </td>
                           <td>
                               {!! Form::open(['route' =>['admin.order.destroy', $order->
                                id]]) !!}
                              
                                <button onClick="return confirm('Eliminar registro?')"
                                class="btn btn-danger">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                                {!! Form::close() !!}
                           </td>
                           <td>{{ $order->id }}</td>
                           <td>{{ $order->created_at }}</td>
                           <td>{{ Auth::user()->name . " " . Auth::user()->apellidos }}</td>
                           <td>${{ number_format($order->subtotal,2) }}</td>     
                           <td>${{ number_format(0,2) }}</td>     
                           <td>${{ number_format($order->total,2) }}</td>  
                       </tr>
                       @endforeach
                   </tbody>
                </table>
            </div>
            <hr>

            <?php echo $orders->render(); ?> <!--  codigo para que nos gener ala paginacion -->

        </div>
    </div>
    
    <!-- Herencia modal-detalle-pedido-->
    @include('admin.partials.modal-detalle-pedido')

@stop
<!--Contenido para sobrescribir -->