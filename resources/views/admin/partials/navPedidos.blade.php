<!--
|--------------------------------------------------------------------------------
| NavPedidos
|--------------------------------------------------------------------------------
| Página html que funciona como barra de navegacion para vista de pedidos
-->

<!-- Barra de navegación partials-->
<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
  <a class="navbar-brand main-title" href="/homeVeterinaria">Veterinaria</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor01">
        <p class="navbar-text"><i class="fa fa-dashboard"></i> Dashboard</p>
      <ul class="navbar-nav mr-auto">
        <li class="nav-item">
          <a class="navbar-text" href="{{ url('index') }}" >Catalogo Artículos</a>
        </li>
     
      </ul>
      <ul class=" navbar-nav dropdown ">
        
        <li><a class="nav-link" href="#">Pedidos</a></li>
        

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" 
              data-toggle="dropdown" 
              href="#" 
              role="button" 
              aria-haspopup="true" 
              aria-expanded="false"
              >
              <i class="fa fa-user"></i>{{ Auth::user()->name }}<span class="caret"></span> 
            <!--  <i class="fa fa-user"></i>Admin<span class="caret"></span>  -->
            </a>
            <ul class="dropdown-menu" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 37px, 0px);">
            <li><a class="dropdown-item " href="{{ route('logout') }}">Finalizar sesión</a></li>
            
            </ul>
        </li> 
      </ul> 
    </div>
    <!-- //.Barra de navegación partials-->

</nav>
<!-- Barra de navegación partials-->