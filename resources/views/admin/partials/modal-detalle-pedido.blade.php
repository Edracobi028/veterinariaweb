<!--
|--------------------------------------------------------------------------------                    
| Modal-Detalle-Pedido
|--------------------------------------------------------------------------------
| Página html que despliega un modal con detalle de pedidos seleccionado por el usuario
-->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Detalle del Pedido: </h4>
      </div>
      <div class="modal-body">
        
        <div class="table-responsive">
          <table class="table table-stripped table-bordered table-hover" id="table-detalle-pedido"><!--tabla a la que le agregaremos todas las filas que obtenga desde la peticion a ajax-->
            <thead>
              <tr>
                <th>Imagen</th>
                <th>Producto</th>
                <th>Precio</th>
                <th>Cantidad</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
               
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
             
            
            </tbody>  
          </table>
        </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>
<!-- //.Modal -->