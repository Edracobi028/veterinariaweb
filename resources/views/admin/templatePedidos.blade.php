<!--
|--------------------------------------------------------------------------------
| TemplatePedidos
|--------------------------------------------------------------------------------
| Paáina html que funciona como plantilla de headerd de pagina web
-->
<!DOCTYPE html>
<html lang="es">
 <!-- Head -->   
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">  <!--jsnode-->
    <title>Veterinaria - Dashboard</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/lumen/bootstrap.min.css" rel="stylesheet" integrity="sha384-iqcNtN3rj6Y1HX/R0a3zu3ngmbdwEa9qQGHdkXwSRoiE+Gj71p0UNDSm99LcXiXV" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Lobster+Two|Poiret+One&display=swap" rel="stylesheet">
    <link rel="stylesheet" href=" {{ asset('admin/css/main.css')}}" >    
    
</head>
<!-- //.Head -->  

<!-- Body o cuerpo de información-->
<body>

    <!-- Alertas -->
    @if(\Session::has('message'))
        @include('admin.partials.message')
    @endif  
    
    <!--   <div id="app">  jsnode-->
    @include('admin.partials.navPedidos')

    <!--Contenido para sobrescribir -->
    @yield('content')

    <!-- Herencia para el footer-->
    @include('admin.partials.footer')
    <!--  <example-component></example-component>  jsnode-->
    <!--  </div>  jsnode-->
    <!--  <script src="js/app.js" ></script>   jsnode    nota: se comentó x que afecta el dropdown --> 
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>   <!--jsnode-->
    <script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" 
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
    <script src="{{ asset('admin/js/main.js') }}"></script>
   
</body>
<!-- //.Body o cuerpo de información-->

</html>