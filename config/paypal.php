<?php
/*
|--------------------------------------------------------------------------------
| Paypal (configuración)
|--------------------------------------------------------------------------------
| Archivo php que sirve para configurar el client y el secret proporcionada l utilizar un api de Paypal
*/

return array(
    //Set your paypal credential
    'client_id' => 'AbwSZInhDpz8BMWJeo5mMRui5BcO2sC0oXS_nIgaqqCTfBR51e6tuaLCZA8-012awohEL2fDDk08NQkB',
    'secret' => 'ENuqjwZyoui3nv-2HgCYeJUC1I60uRs0lq_gPeZGfm2zIvsLJ0x3I_M-kCzRWuKGyxjHWL1HGmiG513L',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Opción disponible 'sandbox' o 'live'
         */
        'mode' => 'sandbox',

        /**
         * Especificar el tiempo de espera maximo en segundos
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Si desea iniciar sesión en un archivo
         */
        'log.LogEnabled' => true,

        /**
         * Especifica el archivo donde quiere escribir.
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Opciones disponibles 'FINE', 'INFO', 'WARN' o 'ERROR'
         *
         * El registro es más detallado en el nivel 'FINO' y disminuye 
         * a medida que avanza hacia ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);