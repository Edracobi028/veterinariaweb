<?php
//use Illuminate\Support\Facades\Route;

/*
|-------------------------------------------------------------------------------
| Web Routes
|-------------------------------------------------------------------------------
| Este archivo recibe todas las peticiones que se hacen a nuestro sitio
| Aquí es donde puede registrar rutas web para su aplicación. 
*/

/*Ruta para buscar mediante el slug el item a interactuar en base de datos 
  y nos traiga sus datos*/
Route::bind('product', function($slug){
       return App\Product::where('sku', $slug)->first();
    });

/* Comentado por resguardo */
//new
//Route::get('/', [
//    'as' => 'home',
//    'uses' => 'StoreController@index'
//]);

/* Ruta para mostrar vista la vista carrito accediendo al método home*/
Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@home'
]);

/* Ruta para mostrar vista la vista carrito accediendo al método homeVeterinaria*/
Route::get('/', function () {
      return view('homeVeterinaria');
});


/********************************************************
*                      Login               
*********************************************************/

Route::get('/login', [
    'as' => 'login',
    //'uses' => 'StoreController@index'
]);

/* Ruta para mostrar la vista para registrarse */
Route::get('/registrarUsuario', function () {
    return view('auth.register');
});

//new 
/*Ruta donde cada vez que encuentre un producto y su slug nos muestre sus tetalle 
  accediendo al metodo "show"*/
Route::get('product/{sku}', [
    'as' => 'product-detail',
    'uses' => 'StoreController@show'
]);


/********************************************************
*                      Carrito               
*********************************************************/
/* Ruta para mostrar vista la vista carrito accediendo al método show*/
Route::get('cart/show', [
    'as' => 'cart-show',
    'uses' => 'CartController@show'
]);

/* Ruta para mostrar vista accediendo al método add*/
Route::get('cart/add/{product}', [
    'as' => 'cart-add',
    'uses' => 'CartController@add'
]);

/* Ruta para mostrar vista accediendo al método delete*/
Route::get('cart/delete/{product}', [
    'as' => 'cart-delete',
    'uses' => 'CartController@delete'
]);

/* Ruta para mostrar vista accediendo al método trash*/
Route::get('cart/trash', [
    'as' => 'cart-trash',
    'uses' => 'CartController@trash'
]);

/* Ruta para mostrar vista accediendo al método update*/
Route::get('cart/update/{product}/{quantity?}', [
    'as' => 'cart-update',
    'uses' => 'CartController@update'
]);

/*ruta para verificar si estamos logeados e irnos a detalle de pedido*/
Route::get('order-detail', [
    //'middleware' => 'auth',
    'as' => 'order-detail',
    'uses' => 'CartController@orderDetail'
]);

/* Ruta para mostrar vista accediendo al método index */
Route::get('index', [
    'as' => 'index',
    'uses' => 'StoreController@index'
]);


/********************************************************
*                      PayPal               
*********************************************************/

/*  Eviamos nuestro pedido a PayPal */
Route::get('payment', array(
    'as' => 'payment',
    'uses' => 'PaypalController@postPayment',
));
/*EFECTIVO */
Route::get('paymentEfectivo', array(
    'as' => 'paymentEfectivo',
    'uses' => 'PaypalController@paymentEfectivo',
));

/*  Paypal redirecciona a esta ruta */
Route::get('payment/status', array(
    'as' => 'payment.status',
    'uses' => 'PaypalController@getPaymentStatus',
));


/********************************************************
*                ADMIN - Panel de pedidos                *
*********************************************************/

/*Esta es la peticion para listar en nuestro panel de pedidos*/
Route::get('admin/orders',[ 
    'as' => 'order.indexPedidos',
    'uses' => 'Admin\OrderController@index'
]);

/* Ruta para mostrar vista accediendo al método getItems */
Route::post( 'admin/order/get-items', [
    'as' => 'admin.order.getItems',
    'uses' => 'Admin\OrderController@getItems'
]);

/* Ruta para mostrar vista accediendo al método destroy */
Route::get('order/{id}', [
    'as' => 'admin.order.destroy',
    'uses' => 'OrderController@destroy'
]);

// Peticion original al home del proyecto ------------
/* Ruta para mostrar vista homeVeterinaria */
Route::get('/home', function () {
    return view('homeVeterinaria');
});

/* Ruta para mostrar vista index */
Route::get('/indexCatalogo', function () {
    return view('index');
});


/********************************************************
*                RUTAS PEDIDO A TIENDA                  *
*********************************************************/
/* Ruta genérica para mostrar vista  */
Route::get('/pedidos/consulta', 'PedidoController@index');

/* Ruta para mostrar vista dashboard */
Route::get('/dashboard', function () {
    return view('dashboard.dashboard');
});




Auth::routes();

/* Ruta para mostrar vista homeVeterinaria */
Route::get('/homeVeterinaria', 'HomeController@homeVeterinaria');

/* Ruta para mostrar vista plantilla */
Route::get('/plantilla', 'HomeController@plantilla');

/* Ruta para mostrar vista plantilla */
Route::get('/loginModal', 'HomeController@loginModal');

/* Ruta para mostrar vista registrarMascota */
Route::get('/registrarMascota', function ()
{
    return view('store/registrarMascota');
});

/* Ruta para mostrar vista indexCatalogo */
Route::get('/indexCatalogo', 'StoreController@indexCatalogo');

/* Ruta para mostrar vista citas */
Route::get('/citas', 'HomeController@citas');

/* Ruta para mostrar vista vacunas */
Route::get('/vacunas', 'HomeController@vacunas');

/* Ruta para mostrar vista orders */
Route::get('/orders', 'StoreController@orders');

