/*
|--------------------------------------------------------------------------------
| MainStore
|--------------------------------------------------------------------------------
| Archivo de tipo JavaScript que agrega funcionalidad a la parte de la pagina web correspondiente
*/

/**
 * Método que aplica estilo tipo Printerest...
 * 
 * @description Método para aplicar estilo, solo a las secciones "products" ..
 */ 
$(document).ready(function() {
    $('#products').pinterest_grid({
        no_columns: 4,
        padding_x: 10,
        padding_y: 10,
        margin_bottom: 50,
        single_column_breakpoint: 700
    });

    /**
     * @description Método para actualizar el item de un carrito, aplica para 
     * todos los elementos que tiene la clase btn aquello que este dentro.* 
     */ 
    $(".btn-update-item").on('click', function(e){ //cuando le demos clic a un elemento de esta clae
        e.preventDefault();                         //quitar la predeterminada del enlace

        var id = $(this).data('id');                //obtengo el valor del id que guarde
        var href = $(this).data('href');            //obtengo la url en el dat href
        var quantity = $("#product_" + id).val();   //obtengo la cantidad
        
        /*redireccionar a href y le pongo la cantidad y poder agreagrle a mi url esta cantidad*/
        window.location.href = href + "/" + quantity;
        
    });

});
