<?php
/*
|--------------------------------------------------------------------------------
| OrderController
|--------------------------------------------------------------------------------
| Archivo PHP que funciona como controlador de los pedidos.
*/

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\OrderItem;

/**
 * En esta clase es donde administramos los pedidos.
 * en el puedes encontrar metodos como: index(), getItems(), destroy($id).
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com	
 */
class OrderController extends Controller
{

    /**
     * Método donde vamos a obtener todos nuestro productos .
     * @return view
     */
    public function index()//
    {
        $orders = Order::orderBy('id', 'desc')
                        ->where('id_users','=',\Auth::user()->id)
                        ->paginate(5); //atravez del modelo obtenemos los pedidos en 
                        //orden por el id haciendo una paginacion de 5 en 5
                        //dd($orders);                                        
        return view('admin.order.indexPedidos', compact('orders'), [ "scrollLinks" => true]);
    }//.index

    /**
     * Método para obtener los items del pedido y pasandole $request devuelve 
     * parametro $items. 	
     * @return json_encode
     */ 
    public function getItems(Request $request)
    {
        /*esta consulta te devuelve todos los items del producto */
        $items = OrderItem::with('product')->where('id_pedido', $request->get('id_pedido'))->get();
        return json_encode($items);
    }//.getItems

    /**
     * Método para eliminar los items del pedido y pasandole id y te devuelve vista. 	
     * @return view
     */ 
    public function destroy($id)
    {
        $order = Order::findOrFail($id);

        $deleted = $order->delete();

        $message = $deleted ? 'Pedido eliminado correctamente!' : 'El pedido NO pudo eliminarse!';

        return redirect()->route('admin.order.indexPedidos')->with('message', $message);
    }//.destroy

}//.OrderController
