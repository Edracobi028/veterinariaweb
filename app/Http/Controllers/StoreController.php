<?php
/*
|--------------------------------------------------------------------------
| Store Controller 
|--------------------------------------------------------------------------
| Archivo php donde se administra el controlador de el catálogo.
*/

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Product; // para mostrar los productos uso el modelo "Product"

/**
 * @desc En esta clase es donde llamamos metodos para que nos devuelvan vistas,
 * puedes encontrar métodos como index(), show(), ordes(), misPedidos().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class StoreController extends Controller
{
    
    /**
     * Metodo index que regresa una vista
     * @return view
     * @desc Método para obtener informacion de la base de datos relacionada con los productos
     */ 
    public function index ()
    {
        // Atravez del modelo estoy obteniendo todos mis productos y los guardo 
        //en la variable $products
        $products = Product::all(); 
        
        //dd($products); //herramienta de laravel para hacer debug

        /*Llamar a la vista y pasarle a la vista index el array los productos*/
        return view('store.index', ["products" => $products, "scrollLinks" => true]); 
        //scrolllinks = true es la propiedad para copiar en un contenido parecido a articulos aporte alex
    }//.index

    /**
     * Método show que regresa una vista de detalle de producto
     * @return view
     * @param $slug
     * @desc Método para obtener informacion de la base de datos relacionada con los productos del slug que le
     * estamos pasando 
     */ 
    public function show ($slug)
    {
        /* Consulta que compara el slug contra el sku de la base de datos*/
        $product = Product::where('sku', $slug)->first();
        //dd($product);    

        return view ('store.show', compact('product'),[ "scrollLinks" => true]); //aporte
    } //.show

    /**
     * Método orders que regresa una vista
     * @return view
     * @desc Método para trabajar en la vista de pedidos.
     */ 
    function orders()
    {
        return view('admin.store.orders1');
    }//.orders

    /**
     * Método misPedidos que regresa una vista
     * @return view
     * @desc Método para para visualizar mis pedidos.
     */ 
    //prueba 
    function misPedidos(){
        $pedidos = Pedido::where("idCliente", Cliente::getUser()->id)->get();
        return view("website.misPedidos",[
            "pedidos" => $pedidos
        ]);
    }//.misPedidos

}//.StoreController