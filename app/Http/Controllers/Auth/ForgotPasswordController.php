<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | Este controlador es responsable de manejar los correos electrónicos
    | de restablecimiento de contraseña e incluye un rasgo que ayuda a
    | enviar estas notificaciones desde su aplicación a sus usuarios.
    | Siéntase libre de explorar este rasgo.
    */

class ForgotPasswordController extends Controller
{
    

    use SendsPasswordResetEmails;

    /**
     * Crea una nueva instancia de controlador.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
}
