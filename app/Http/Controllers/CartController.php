<?php
/*
|--------------------------------------------------------------------------------
| Controlador de carrito (CartController)
|--------------------------------------------------------------------------------
| Archivo php que controlla los metodos para administrar el carrito
*/
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Product;

/**
 * @desc En esta clase es donde encontraras metodos para administrar el carrito 
 * como _construct, show(), add(), delete(), update(), trash(), total(),orderDetail(),
 * indexCatalogo().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class CartController extends Controller
{
    /**
     * Método ...
     * @return void
     * @desc Método que funciona como constructor de este controlador..
     */ 
    public function __construct()
    {
        /* Crear la variable de sesión carrrito condicionando si no existe */
        if(!\Session::has('cart')) \Session::put('cart',array());
    }

    /**
     * @return view
     * @description Método para mostrar el carrito pasandole carrito y total..
     */
    public function show()
    {
        /*crear variable cart y guardar variable de sesion*/
        $cart = \Session::get('cart'); 

        /* guadar en variable lo que nos devielva el metodo total()*/
        $total = $this->total();

        /*mostrar vista pasando todo lo que hay en la variable cart */
        return  view('store.cart', compact('cart', 'total'),[ "scrollLinks" => true]);
    }


    /**
     * 
     * @return view cart-show
     * @desc Método para agregar un producto al carrito.
     */
    public function add(Product $product)
    {
        /*recibe la variable de sesion y la guardamos en local*/
        $cart = \Session::get('cart'); 

        /*Para mostrar la cantidad default en "1" */
        $product->quantity = 1; 

        /*agregar todo la info del objeto $product en nuestro array "cart" utilizando sku como indice */
        $cart[$product->sku] = $product; 

        /* Volver a actualizar mi variable de sesión */
        \Session::put('cart',$cart);

        /* redireccionar hacia vista show */
        return redirect()->route('cart-show');
    }

    /**
     * @return view cart-show 
     * @desc Método para eliminar un producto del carrito recibiendo un objeto de 
     * la clase producto.
     */
    public function delete(Product $product)
    {
        /*guardamos en el arreglo cart la variable de sesion actual*/
        $cart = \Session::get('cart');

        /* con el metodo unset eliminamos del arreglo cart mediante el indice que nos de esl slug */
        unset($cart[$product->sku]);

        /*actualizamos la variable de sesion*/
        \Session::put('cart',$cart); 

        /* redireccionar hacia vista show */
        return redirect()->route('cart-show');
    }

    /**
     * @return view
     * @desc Método para actualizar el carrito, recibe el argumento del producto y la cantidad
     */
    public function update(Product $product, $quantity)
    {
        /*guardamos en el arreglo cart la variable de sesion actual*/
        $cart = \Session::get('cart');

        /*agregar todo la info del objeto $quantity en  array "cart" utilizando sku como indice */
        $cart[$product->sku]->quantity = $quantity;

        /*actualizamos la variable de sesion*/
        \Session::put('cart', $cart); 

        /* redireccionar hacia vista show */
        return redirect()->route('cart-show');
    }

    /** 
     * @return view cart-show
     * @desc Método para vaciar el carrito..
     */
    public function trash()
    {
        /*con la clase forget elimina lo que contega la variable cart*/
        \Session::forget('cart');

        /* redireccionar hacia vista show */
        return redirect()->route('cart-show');
    }

    /**
     * @return $total
     * @desc Método para obtener el total a pagar por los artículos..
     */
    private function total()
    {
        /*guardamos en el arreglo cart la variable de sesion actual*/
        $cart = \Session::get('cart');

        /*creamos una variable total y la inicializamos en ceros*/
        $total = 0; 

        /* rrecorremos y obtenemos el subtotal (precio * cantidad)  sumando a total */
        foreach($cart as $item){ 
            $total += $item->precio_venta * $item->quantity; 
        }
        return $total; //retorna lo que tendriamos que pagar
    }

    /**
     * @return view
     * @desc Método para mostrar el detalle de pedido, validando si ya tiene articulos
     * agregados en el carrito.
     */
    public function orderDetail()
    {
        /*si el numero items que hay en la variable de carrito es menor o igual a cero lo direccionamos a home */
        if(count(\Session::get('cart')) <= 0) return redirect()->route('index');
        $cart = \Session::get('cart');          //Guardar lo que hay en el carrito
        $total = $this->total();
        return view ('store.order-detail', compact('cart', 'total'),[ "scrollLinks" => true]);
        
    }

    /**
     * @return view index
     * @desc Método para mostrar la vista de incio del catálogo..
     */
    public function indexCatalogo()
    {
        return view ('store.index');
    }

    
}
