<?php
/*
|--------------------------------------------------------------------------------
| PaypalController (Controlador de payPal)
|--------------------------------------------------------------------------------
| Archido php en donde administramos todo lo relacionado con el pedido via Paypal
*/
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Validation\ValidatesRequests;
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\OrderItem;
use App\Order;

//use PayPal\Exception\PayPalConnectionException;
/**
 * @desc En esta clase es donde configuraciones y lectores del pedido realizado via 
 * Paypal, en ella encontramos metodos como: __construct(), postPayment(), 
 * getPaymentStatus(), 
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com	
 */
class PaypalController extends Controller
{
    /*creamos una variable privada que va a contener todas las configuraciones*/
    private $_api_context; 

    /**
     * @return void
     * @desc Método que funciona como constructor del controlador PaypalController.
     */ 
    public function __construct()
    {
        //setup PayPal api context
        $paypal_conf = \Config::get('paypal'); //archivo de conf. paypal creado en la carpeta config
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'],
        $paypal_conf['secret']));// se toman el client id y secret para api_context
        $this->_api_context->setConfig($paypal_conf['settings']);
    }//.Construct


    /**
     * @return view cart-show
     * @desc Método Donde se configura todo lo que le enviamos a paypal
     */ 
    public function postPayment()
    {
        $payer = new Payer(); //Se crea un objeto de tipo Payer contine pago del cliente y metodo de pago
        $payer->setPaymentMethod('paypal'); //se configura el metodo del pago atravez del metodo "setPaymentMethod"

        $items = array(); //creamos un array llamado items
        $subtotal = 0; // iniciar nuestro subtotal en ceros
        $cart = \Session::get('cart'); // obtener toda nuestra informacion con el carrito
        $currency = 'MXN'; // Configurar la moneda que vamos a utilizar

        foreach($cart as $producto){ //Por cada producto que hay en nuestro carrito
            $item = new Item(); //creamos un objeto de la clase item
            $item->setName($producto->nombre) //configurar a travez del metodo todos los datos de ese producto 
            ->setCurrency($currency) //el tipo de moneda
            ->setDescription($producto->descripcion) //la descripcion
            ->setQuantity($producto->quantity) //el numero de piezas
            ->setPrice($producto->precio_venta); //el precio

            $items[] = $item; //Aqui lo agregamos la array
            $subtotal += $producto->quantity * $producto->precio_venta; //con esto obtenemos el subtotal
        }//.foreach

        $item_list = new ItemList(); //Creamos un objeto de tipo ItemList
        $item_list->setItems($items); //en el guardarmos o configurar el array que ya traiamos
        $details = new Details(); //se crea un objeto de clase Details
        $details->setSubtotal($subtotal) //sirve para agregar un costo envio con el subtotal
        ->setShipping(0); //y el costo por el envio //Razo modificado 

        $total = $subtotal + 0;  //Razo modificado 
        $amount = new Amount();        // creamos un objeto que va a guardar esas cantidades
        $amount->setCurrency($currency)                             //configuramos la moneda
            ->setTotal($total)                                          // el total a pagar
            ->setDetails($details); // y lo que hay en  objeto details relacionado al envio
        $transaction = new Transaction();     // Creamos una objeto de la clase transaction

        /*aqui le pasamos la cantidad del total a pagar costo de envio y moneda*/
        $transaction->setAmount($amount) 

            /* le pasamos el objeto que contiene todos los items del carrito*/
            ->setItemList($item_list)  

            /*aqui le ponemos una pequeña descripcion*/
            ->setDescription('Pedido de prueba en Veterinaria');  
        
        $redirect_urls = new RedirectUrls(); //Creamos un objeto de la clase "RedirectUrls"
        $redirect_urls->setReturnUrl(\URL::route('payment.status')) //recibela ruta si se acepta el pago
            ->setCancelUrl(\URL::route('payment.status'));  // Oh la ruta si se cancela

        /*creamos un objeto de tipo payment a travez del cual se va a realizar el pago*/
        $payment = new Payment();                                        //crear variable
        $payment->setIntent('Sale')   //configuramos el tipo de pago que sera venta directa
            ->setPayer($payer)                     // le pasamos el objeto de tipo "Payer"
            ->setRedirectUrls($redirect_urls)    //Le pasamos las urls que creamos arriba
            ->setTransactions(array($transaction)); // le pasamos el objeto "transaction"
        
        /*ejecutamos el metodo create de payment y lo metemos en un try catch, aqui se 
        valida la conexion a paypal  */
        try{
            $payment->create($this->_api_context); 
            } catch(\Paypal\Exception\PPConnectionException $ex) {
                if (\Config::get('app.debug')){
                    echo "Exception: " . $ex->getMessage() . PHP_EOL;
                    $err_data = json_decode($ex->getData(), true);
                    exit;
                } else {
                    die('Ups! Algo salio mal');
                }//.else
            }//.catch

        foreach($payment->getLinks() as $link){
            if($link->getRel() == 'approval_url'){ // el enlace que viene en la respuesta de PayPal

                /* esos atributos los guardamos en la variable "redirect_url" para redireccionar 
                al usuario hacia PayPal*/
                $redirect_url = $link->getHref(); 
                break;
            }
        }//.foreach

        /* añadir el  payment ID a sesion */
        \Session::put('paypal_payment_id', $payment->getId());

        if(isset($redirect_url)){
            return \Redirect::away($redirect_url); // redireccionamos a paypal
        }//.if

        return \Redirect::route('cart-show') // si hay problema, direcciona al carrito
            ->with('message', 'ups! Error desconocido.'); // envio mensaje de error

    }//.postPayment

    /**
     * @return view
     * @desc Método con el cual obtenemos respuesta de Paypal si el pago fue aprovado.
     * Manda a home mostrando un mensaje en caso de ser exitoso o no.
     */ 
    public function getPaymentStatus()
    {
        /* obtenemos la variable de seguimiento payment_id", ya que ya no la utilizaremos*/
        $payment_id = \Session::get('paypal_payment_id'); 
        \Session::forget('paypal_payment_id'); // Limpiar el  payment ID d ela sesion
        $payerId = \Input::get('PayerID');                   //dato que nos da paypal
        $token = \Input::get('token');                       //dato que nos da paypal

        /*si alguna esta vacia algo paso y la transaccion no s ellevo a cabo*/
        if (empty($payerId) || empty($token)) { 
            return \Redirect::route('index') //y nos direcciona a homey muestra este mensaje
                ->with('message', 'Hubo un problema al intentar pagar con Paypal' );  
        }//.if

        /*si todo arriba salio bien obtenemos el objeto "payment"*/
        $payment = Payment::get($payment_id, $this->_api_context); 

        /*creamos un nuevo objeto de tipo "PaymentExecution"*/
        $execution = new PaymentExecution();  

        /* le configuramos el Payer id que nos trajo paypal*/
        $execution->setPayerId(\Input::get('PayerID')); 

        /* al ejecutar este metodo es cuando se realiza la transaccion completa*/
        $result = $payment->execute($execution, $this->_api_context); 
        
        /*si nos devuelve el estado aprovado y se guarda en el objeto "result"*/
        if ($result->getState() == 'approved') { 
            $this->saveOrder();                      // metodo para guardar la info del pedido
            \Session::forget('cart');            // Eliminamos el carrito, ya no lo ocuparemos 
            return \Redirect::route('index')                           //nos direcciona a home 
            ->with('message', 'Compra realizada de forma correcta'); // nos muestra el mensaje
        }//.if

        return \Redirect::route('index')                  //caso contrario nos redirije a home
            ->with('message', 'La compra fue cancelada');                  // Con este mensaje

    }//.getPaymentStatus

    public function paymentEfectivo()
    {

        $subtotal = 0;
        $cart = \Session::get('cart'); //obtener la variable de sesion en cart
        $shipping = 0;  // costo por el envio

        foreach($cart as $producto){
            $subtotal += $producto->quantity * $producto->precio_venta;
        }//.foreach
        //dd(\Auth::user()->id);
        
        /*Atravez del modelo order creo un registro al cual le pasaremos los sig. datos */
        $order = Order::create([  
            'tipoPago' => 'paypal', 
            'estado' => 'Aprobado',
            'idPago' => '01',
            'subtotal' => $subtotal,
            'total' => $subtotal + $shipping,
            'descuento' => '0',
            'estadoPago' => 'autorizado',
            //'extras' => $shipping,
            'id_users'  => \Auth::user()->id // el id del user a travez del modelo Auth
            
        ]);

        /* recorre  el carrito */    
        foreach($cart as $producto){  

            /*por cada item llamamos al metodo obteniendo los datos y crear registro*/
            $this->saveOrderItem($producto, $order->id);  
        }//.foreach
        \Session::forget('cart');  //vacia el carrito
        return \Redirect::route('index'); // nos muestra el mensaje
    }

    /**
     * @return void
     * @desc Método para guardar el pedido..
     */
    protected function saveOrder()
    {
        $subtotal = 0;
        $cart = \Session::get('cart'); //obtener la variable de sesion en cart
        $shipping = 0;  // costo por el envio

        foreach($cart as $producto){
            $subtotal += $producto->quantity * $producto->precio_venta;
        }//.foreach
        //dd(\Auth::user()->id);
        
        /*Atravez del modelo order creo un registro al cual le pasaremos los sig. datos */
        $order = Order::create([  
            'tipoPago' => 'paypal', 
            'estado' => 'Aprobado',
            'idPago' => '01',
            'subtotal' => $subtotal,
            'total' => $subtotal + $shipping,
            'descuento' => '0',
            'estadoPago' => 'autorizado',
            //'extras' => $shipping,
            'id_users'  => \Auth::user()->id // el id del user a travez del modelo Auth
            
        ]);

        /* recorre  el carrito */    
        foreach($cart as $producto){  

            /*por cada item llamamos al metodo obteniendo los datos y crear registro*/
            $this->saveOrderItem($producto, $order->id);  
        }//.foreach
    }//.saveOrder

    /**
     * Método ...
     * @return void
     * @desc Método para guardar el articulo ene le pedido..
     */ 
    protected function saveOrderItem($producto, $order_id) //recibe el item y el id del pedido
    {
        /*por medio del modelo OrderItem crea un registro con los sigs. datos*/
        OrderItem::create([ 
            'subtotal' => $producto->precio_venta,
            'cantidad' => $producto->quantity,
            'id_articulo' => $producto->id,
            'id_pedido' => $order_id
        ]);
    }//.saveOrderItem

}//.PaypalController
