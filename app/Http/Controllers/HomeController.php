<?php
/*
|--------------------------------------------------------------------------------
| HomeController
|--------------------------------------------------------------------------------
| Archivo php que administra las funciones del controlador de home
*/ 

namespace App\Http\Controllers;
use Illuminate\Http\Request;

/**
 * En esta clase es donde declaramos metodos para mostrar vistas.
 * En el puedes encontrar metodos para tarer vistas como: construct(), home(), homeVeterinaria(), 
 * plantilla(), loginModal(), citas(), vacunas(), registrarUsuario().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 * 
 * 	
 */
class HomeController extends Controller
{
    /**
     * Crea una nueva instancia del controlador home.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');   //bloqueo de inicio de sesion
    }

    /**
     * Muestra el panel de aplicación home.
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function home()
    {
        return view('homeVeterinaria');
    }
    
    /**
     * Muestra vista.
     * @return view
     */
    function homeVeterinaria()
    {
        return view('homeVeterinaria');
    }//.homeVeterinaria
  
    /**
     * Muestra vista.
     * @return view
     */
    function plantilla()
    {
        return view('plantilla');
    }//.plantilla
    
    /**
     * Muestra vista.
     * @return view
     */
    function loginModal()
    {
        return view('loginModal');
    }//.loginModal

    /**
     * Muestra vista.
     * @return view
     */
    function citas()
    {
        return view('citas');
    }//.citas

    /**
     * Muestra vista.
     * @return view
     */
    function vacunas()
    {
        return view('vacunas');
    }//.vacunas

    /**
     * Muestra vista.
     * @return view
     */
    function registrarUsuario()
    {
        return view('registrarUsuario');
    }//.registrarUsuario



  

    
    
  
    
    
}
