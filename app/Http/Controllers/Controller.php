<?php
/*
|--------------------------------------------------------------------------------
| Controller
|--------------------------------------------------------------------------------
| Archivo php de controlador default de laravel
*/ 

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

/**
 * En esta clase es un controlador default de laravel, aun sin utilizar
 * @return void
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
