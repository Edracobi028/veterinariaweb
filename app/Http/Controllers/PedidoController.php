<?php
/*
|--------------------------------------------------------------------------------
| PedidoController   (Controlador de pedido)
|--------------------------------------------------------------------------------
| Archivo phph donde se administra el controlador de pedido
*/ 

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Order;

/**
 * En esta clase se administran los meodos de controlador de pedido
 * se encuentran metodos como: index(), create(), store(), show(), edit(), update()
 *  y destroy().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 */
class PedidoController extends Controller
{
    /**
     * Desplega una lista de recusros.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $buscar = $request->buscar;
        $criterio = $request->criterio;

        $pedidos = Order::orderBy('id','desc')->paginate(20);

        return[
            'pagination' =>[
                'total' => $pedidos->total(),
                'current_page' => $pedidos->currentPage(),
                'per_page' => $pedidos->perPage(),
                'last_page' => $pedidos->lastPage(),
                'from' => $pedidos->firstItem(),
                'to' => $pedidos->lastItem(),
            ],
            'pedidos' =>$pedidos
        ];

    }//.index

    /**
     * Muestra el formulario para crear un nuevo recurso.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }//.create

    /**
     * Almacenar un recurso recién creado en almacenamiento.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }//.store

    /**
     * Mostrar el pedido especificado.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }//.show

    /**
     * Muestra el formulario para editar el recurso especificado.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }//.edit

    /**
     * Actualiza el pedido especificado en la base de datos.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }//.update

    /**
     * Elimina el pedido especificado en la base de datos.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }//.destroy
}//.PedidoController
