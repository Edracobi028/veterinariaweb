<?php
/*
|--------------------------------------------------------------------------
| Modelo User
|--------------------------------------------------------------------------
| Este archivo php, funciona para insertar datos a las tablas mediante los modelos.
*/

/**
 * @desc En esta clase es el modelo de usuarios default de laravel ,
 * encontrarás metodos como orders(), y arrays como $filiable, $hidden, $casts.
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com	
 */
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * Los atributos que son asignables en masa.
     *
     * @var array
     */
    protected $fillable = [
        //'name','last_name', 'email', 'user', 'password', 'type','active','address',
        'name','apellidos','domicilio','curp','rfcCliente','telefono','email','password','created_at'
        
    ];

    /**
     * Los atributos que deben ocultarse para las matrices.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Los atributos que se deben convertir a tipos nativos.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * Método para hacer relaciones entre modelos ...
     * @return hasMany
     * @desc Relacion con orders(pedidos). Cada usuario puede tener uno o mas pedidos ..
     */

    public function orders()
    {
        return $this->hasMany('App\Order');//cada usuario puede tener uno o mas pedidos
    }//.orders
    
}//.User
