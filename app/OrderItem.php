<?php
/*
|--------------------------------------------------------------------------------
| OrderItem
|--------------------------------------------------------------------------------
| Archivo php que administra los items que le pertenecen a un pedido
*/
namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * @desc En esta clase creamos relaciones entre modelos y llenado 
 * de tabla en base de datos. En el encontramos metodos como: order(), product().
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com	
 */
class OrderItem extends Model
{
    //protected $table = 'order_items' ;         //se crea una variable llamada order
    protected $table = 'articulos_pedidos' ;     //se crea una variable llamada order
    protected $fillable = ['cantidad','subtotal', 'id_pedido','id_articulo'];
    public $timestamps = false; //se ponen en falso porque en esta tabla no se usaran
    protected $dateFormat = 'Y-m-d H:i:s.v';                       //formato de fecha

    /**
     * @return belongsTo
     * @desc Método para hacer la Relacion entre items y productos, cada uno de los  
     * items pertenecerá a un pedido.
     */ 
    public function order()
    {
        return $this->belongsTo('App\Order'); 
    }//.order

    /**
     * @return belongsTo
     * @desc Método para hacer la Relacion entre productos y items.
     */ 
    public function product()
    {
        return $this->belongsTo('App\Product'); 
    }//.product

}//.OrderItem
