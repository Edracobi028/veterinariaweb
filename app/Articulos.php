<?php
/*
|--------------------------------------------------------------------------------
| Modelo Articulos 
|--------------------------------------------------------------------------------
| Archivo php para administrar el modelo de articulos.
*/

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 * @desc En esta clase declaramos las variables y el llenado de la tabla articulos 
 * en base de datos. 
 */
class Articulos extends Model
{
    //Declaracion de variables 
    protected $table = 'articulos' ;  //se crea una variable llamada articulos
    
    protected $fillable = ['sku','descripcion','precio_Venta','impuestos','ubicacion',
            'precioReal','nombre','marca','presentacion', 'imagen','tipoUnidad',
            'precioUnidad','departamento','unidadMax','existencias',
            'piezasCotizacion','precioCotizacion','creado','id_empleado'
        ];

    public   $timestamps = false; //se ponen en falso porque en esta tabla no se usaran

}//.Articulos
