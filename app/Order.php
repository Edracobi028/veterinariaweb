<?php
/*
|--------------------------------------------------------------------------------
| Modelo Order
|--------------------------------------------------------------------------------
| Archivo php donde se administra las funciones que emplea el modelo Pedido
*/

namespace App;
use Illuminate\Database\Eloquent\Model;

/**
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 * @desc En esta clase creamos relaciones entre modelos y llenado 
 * de tabla en base de datos. En el encontramos metodos de relación como: user(), order_items(). 	
 */
class Order extends Model
{
    protected $table = 'pedidos' ; //se crea una variable llamada pedidos

    /*escribiremos sobre estos campos declarados como un array dentro de la propiedad "fillable"*/
    protected $fillable = ['tipoPago','estado','idPago','subtotal','total','descuento','estadoPago','id_users',
            'created_at'
        ];

    //public   $timestamps = false;
    protected $dateFormat = 'Y-m-d H:i:s';  //formato de fecha

    /**
     * @return belongsTo
     * @desc Método para hacer la Relacion entre Order y User, esta relcion nos permitira 
     * acceder a sus pedidos.
     */ 
    public function user()
    {
        return $this->belongsTo('App\User');//cada pedido va a pertenecer a un usuario
    }//.user

    /**
     * @return hasMany
     * @desc Método para hacer la Relacion entre pedidos e items de ese pedido
     */ 
    public function order_items()
    {
        return $this->hasMany('App\OrderItem'); //cada pedido va a tenr varios items
    }//.order_items


    
}
