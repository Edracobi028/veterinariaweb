<?php
/*
|--------------------------------------------------------------------------
| Modelo Producto
|--------------------------------------------------------------------------
| Este archivo es para insertar datos a las tablas mediante los modelos.
*/

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\BaseModel;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @author Hugo Torres Kronox_@hotmail.com | Eduardo Razo esse_rios_e@hotmail.com
 * @desc En esta clase funciona como modelo creamos relaciones entre modelos y llenado 
 * de tabla en base de datos. En el encontramos metodos como: category(), order_item.
 */
class Product extends Model
{
    protected $table = 'articulos' ;                //Declara el nombre de la tabla
    protected $fillable = ['nombre','descripcion','sku','precio_venta','impuestos','imagen','tipoUnidad','unidadMax',
            'existencias','estatus', 'id_empleado','id_marca','id_presentacion','id_departamento'
        ];

    public   $timestamps = false;             //bloquear los created_at, updated_at
    protected $dateFormat = 'Ymd H:i:s.v';                       //formato de fecha
    
    /**
     * @return belongsTo
     * @desc Método para relacionar con producto pertenece a categoria.
     */ 
    public function category()
    {
        return $this->belongsTo('App\Category');
    }//.category

    /**
     * @return HasOne
     * @desc Método para hacer la relacion  producto tiene un orderItem .
     */
    //Relacion con OrderItem ()
    public function order_item()
    {
        return $this->hasOne('App\orderItem');
    }//.order_item

}//.Product