const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/** 
mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');
*/
mix.styles([
    'resources/assets/dashboard/css/all.min.css',
    'resources/assets/dashboard/css/dataTables.bootstrap4.css',
    'resources/assets/dashboard/css/sb-admin.css'
    ],
    
    'public/css/plantilla.css')//Archivo resultante

    .scripts([
    'resources/assets/dashboard/js/jquery.js',
    'resources/assets/dashboard/js/bootstrap.bundle.js',
    'resources/assets/dashboard/js/jquery.easing.js',
    'resources/assets/dashboard/js/sb-admin.js',
    ],
    'public/js/plantilla.js')//Archivo resultante

    .js(['resources/js/app.js'],'public/js/app.js');//Archivo resultante de vue
     //vue js
